package com.example.demo.likes;

import com.example.demo.common.enumeration.LikeTypeEnum;
import com.example.demo.likes.domain.Likes;
import com.example.demo.likes.service.LikesService;
import com.example.demo.post.domain.Post;
import com.example.demo.reply.domain.Reply;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LikesTest {

    private int ZERO = 0;

    @Autowired
    private LikesService likesService;

    @Test
    public void insertLikes_Post_Test() {
        Likes likes = new Likes();
        likes.setUserNum(1);
        likes.setLikeType(LikeTypeEnum.POST);
        likes.setTypeValue(1);

        int expect = likesService.insertLikes(likes);

        assertThat(expect).isGreaterThan(0);
        System.out.println(expect);
    }

    @Test
    public void getLikesFromReply_Test() {
        int userNum = 1;
        int replyNum = 1;

        Likes expectLikes = new Reply().getLikesFromReply(userNum, replyNum);

        assertThat(expectLikes.getUserNum()).isEqualTo(userNum);
        assertThat(expectLikes.getLikeType()).isEqualTo(LikeTypeEnum.REPLY);
        assertThat(expectLikes.getTypeValue()).isEqualTo(replyNum);
    }

    @Test
    public void insertLikes_Reply_Test() {
        Likes likes = new Likes();
        likes.setUserNum(1);
        likes.setLikeType(LikeTypeEnum.REPLY);
        likes.setTypeValue(1);

        int expect = likesService.insertLikes(likes);

        assertThat(expect).isGreaterThan(0);
        System.out.println(expect);
    }

    @Test
    public void SelectLikesCountByPost_Test() {
        int postNum = 1;

        int expect = likesService.selectLikesCountByPost(postNum);

        assertThat(expect).isGreaterThan(ZERO);
    }
}
