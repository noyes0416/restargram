package com.example.demo.post;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.post.controller.PostController;
import com.example.demo.post.domain.Post;
import com.example.demo.post.persistence.PostMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PostTest {

    @Autowired
    private PostMapper postMapper;

    @Autowired
    private PostController postController;

    @Test
    public void SelectPostListAll_Test() {
        int userNum = 1;

        List<Post> expectPostList = postMapper.selectAllPostList(userNum);
        Post expectPost = expectPostList.get(expectPostList.size() - 1);

        assertThat(expectPost.getNum()).isEqualTo(1);
        assertThat(expectPost.getPostLikedFlagByUser()).isEqualTo(true);
    }

    @Test
    public void InsertPost_GetPK_Test() {
        Post post = new Post();
        post.setUserNum(1);
        post.setContent("InsertPost_GetPK_Test");
        post.setState(ActiveStateEnum.ACTIVE);

        postMapper.insertPost(post);

        assertThat(post.getNum()).isGreaterThan(0);
        System.out.println("post.getNum() : " + post.getNum());
    }

    @Test
    public void InsertPost_Test() {
        int userNum = 1;
        String content = "InsertPost_Test";

        Post post = new Post();
        post.setUserNum(userNum);
        post.setContent(content);
        post.setState(ActiveStateEnum.ACTIVE);

        postMapper.insertPost(post);

        List<Post> postList = postMapper.selectAllPostList(userNum);
        Post expectPost = postList.get(0);
        assertThat(expectPost.getContent()).isEqualTo(content);
    }

    @Test
    public void GetPostDetail_Test() {
        Map hashMap = new HashMap<>();
        int userNum = 1;
        int postNum = 1;

        hashMap.put("userNum", userNum);
        hashMap.put("postNum", postNum);

        Post expectPost = postMapper.selectPostDetail(hashMap);
        assertThat(expectPost.getNum()).isEqualTo(userNum);
    }
}
