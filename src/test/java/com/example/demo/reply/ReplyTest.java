package com.example.demo.reply;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.reply.domain.Reply;
import com.example.demo.reply.persistence.ReplyMapper;
import com.example.demo.reply.service.ReplyService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ReplyTest {

    private int ZERO = 0;

    @Autowired
    private ReplyMapper replyMapper;

    @Autowired
    private ReplyService replyService;

    @Test
    public void SelectReplyListAll_Test() {

    }

    @Test
    public void InsertReply_Test() {
        String replyContent = "#WOW #ISIT?";
        Reply reply = new Reply();
        reply.setContent(replyContent);

        replyService.insertReply(reply);
    }

}
