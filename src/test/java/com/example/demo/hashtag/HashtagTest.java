package com.example.demo.hashtag;

import com.example.demo.common.utils.Utils;
import com.example.demo.hashtag.domain.Hashtag;
import com.example.demo.hashtag.service.HashtagService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HashtagTest {

    @Autowired
    private HashtagService hashtagService;

    @Test
    public void selectHashtagByPost_Test() {
        int postNum = 1;

        List<Hashtag> expectList = hashtagService.selectHashtagByPost(postNum);

        assertThat(expectList.get(0).getNum()).isGreaterThan(0);
    }

    @Test
    public void selectHashtagDetail_Test() {
        int hashtagNum = 1;

        Hashtag expect = hashtagService.selectHashtagDetail(hashtagNum);

        assertThat(expect.getNum()).isEqualTo(hashtagNum);
    }

    @Test
    public void getHashtagFromString() {
        String content = "#Post This is just content #Test #Hashtag YES YES";

        System.out.println(Utils.SplitContentFromString(content));
        Utils.SplitHashtagListFromString(content).forEach(s ->
                System.out.println(s.getValue()));
    }

    @Test
    public void test() {
        String content = "%s this is test %s";
        String [] hashtags = new String[] {"#POST1", "#POST2"};

        System.out.println(String.format(content, hashtags));
    }
}
