package com.example.demo.follow;

import com.example.demo.follow.domain.Follow;
import com.example.demo.follow.persistence.FollowMapper;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class FollowTest extends TestCase {

    @Autowired
    private FollowMapper followMapper;
//
//    @Test
//    public void followerList() {
//        int num = 1;
//        Follow follow = new Follow();
//        follow.setRUserNum(num);
//
//        List<Follow> followerList = followMapper.selectFollowerList(num);
//        Follow expectFollow = followerList.get(0);
//        assertThat(expectFollow.getSender()).isEqualTo(2);
//    }
//
//    @Test
//    public void followingList() {
//        int num = 1;
//        Follow follow = new Follow();
//        follow.setSUserNum(num);
//
//        List<Follow> followingList = followMapper.selectFollowingList(num);
//        Follow expectFollow = followingList.get(0);
//        assertThat(expectFollow.getReceiver()).isEqualTo(2);
//    }

    @Test
    public void followerCount() {
        int num = 1;

        int expectFollower = followMapper.selectFollowerCount(num);
        assertThat(expectFollower).isEqualTo(3);
    }

    @Test
    public void followingCount() {
        int num = 1;

        int expectFollowing = followMapper.selectFollowingCount(num);
//        assertThat(expectFollowing).isEqualTo(1);
    }
}