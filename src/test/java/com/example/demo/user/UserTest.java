package com.example.demo.user;

import com.example.demo.user.domain.User;
import com.example.demo.user.persistence.UserMapper;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserTest extends TestCase {

    @Autowired
    private UserMapper userMapper;

    @Test
    public void list() {
        int num = 2;
        List<User> ussrList = userMapper.selectUserList(num);
        User expectUser = ussrList.get(0);
        assertThat(expectUser.getNum()).isEqualTo(num);
    }

//    @Test
//    public void detail() {
//        int num = 2;
//        User expectUser = userMapper.selectUserDetail(num);
//        assertThat(expectUser.getNum()).isEqualTo(num);
//    }

//    @Test
//    public void signUp() {
//        int num = 7;
//        User user = new User();
//        user.setId("yjs");
//        user.setName("yjshin");
//        user.setEmail("yjs@gmail.com");
//        user.setPassword("qwER");
//        user.setAgreementYn('Y');
//
//        userMapper.insertUser(user);
//
//        User expectUser = userMapper.selectUserDetail(num);
//        assertThat(expectUser.getId()).isEqualTo("yjs");
//    }

    @Test
    public void isUser() {
        User user = new User();
        user.setEmail("two@google.com");
        user.setPassword("qwER");

        User expectUser = userMapper.selectIsUser(user);
        assertThat(expectUser.getName()).isEqualTo("22");
    }


    @Test
    public void signIn() {
        int num = 3;

        User user = new User();
        user.setNum(num);

        userMapper.updateSignIn(user);

        user.setEmail("three@google.com");
        user.setPassword("qwER");

        User expectUser = userMapper.selectIsUser(user);
//        assertFalse(expectUser.getLastLoginDt().isEmpty());
    }
}