package com.example.demo.photo;

import com.example.demo.common.enumeration.PhotoTypeEnum;
import com.example.demo.photo.domain.Photo;
import com.example.demo.photo.persistence.PhotoMapper;
import com.example.demo.photo.service.PhotoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class PhotoTest {

    @Autowired
    private PhotoService photoService;

    @Autowired
    private PhotoMapper photoMapper;

    @Test
    public void insertPhoto_Test() {
        Photo photo = new Photo();
        photo.setOriginName("insertPhoto_Test()");
        photo.setFileName("insertPhoto_Test()");
        photo.setFilePath("sample-post1.png");
        photo.setPhotoType(PhotoTypeEnum.PHOTO);

        photoMapper.insertPhoto(photo);
    }
}
