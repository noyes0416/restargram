package com.example.demo.message;

import com.example.demo.message.domain.Message;
import com.example.demo.message.persistence.MessageMapper;
import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class MessageTest extends TestCase {

    @Autowired
    private MessageMapper mapper;

    @Test
    public void list() {
        int num = 1;
        List<Message> messageList = mapper.selectGroupList(num);
        Message expectMessage = messageList.get(0);
        assertThat(expectMessage.getGroupNum()).isEqualTo(num);
    }
//
//    @Test
//    public void send() {
//        int num = 1;
//        String content = "Test_bbbb";
//        Message message = new Message();
//        message.setGroupNum(num);
//        message.setSender(1);
//        message.setReceiver(2);
//        message.setContent(content);
//
//        mapper.insertMessage(message);
//
//        List<Message> messageList = mapper.selectMessageList(num);
//        Message expectMessage = messageList.get(2);
//        assertThat(expectMessage.getContent()).isEqualTo(content);
//    }
//
//    @Test
//    public void read() {
//        int num = 1;
//        List<Message> messageList = mapper.selectMessageList(num);
//        Message expectMessage = messageList.get(0);
//        assertThat(expectMessage.getGroupNum()).isEqualTo(num);
//    }
//
//    @Test
//    public void sendContent() {
//        int num = 1;
//        Message message = new Message();
//        message.setGroupNum(num);
//        message.setSender(1);
//        message.setReceiver(2);
//        message.setPostNum(num);
//        message.setContent("");
//
//        mapper.insertContent(message);
//
//        List<Message> messageList = mapper.selectMessageList(num);
//        Message expectMessage = messageList.get(3);
//        assertThat(expectMessage.getPostNum()).isEqualTo(num);
//    }
}