package com.example.demo.location;

import com.example.demo.location.domain.Location;
import com.example.demo.location.persistence.LocationMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LocationTest {

    @Autowired
    private LocationMapper locationMapper;

    @Test
    public void selectLocationList_Test() {
        List<Location> expectLocationList = locationMapper.selectLocationList();
        Location expectLocation = expectLocationList.get(expectLocationList.size() - 1);
        assertThat(expectLocation.getNum()).isEqualTo(1);
    }

    @Test
    public void selectLocationByPost_Test() {
        int postNum = 1;

        Location expectLocation = locationMapper.selectLocationByPost(postNum);

        assertThat(expectLocation.getNum()).isGreaterThan(0);
    }
}
