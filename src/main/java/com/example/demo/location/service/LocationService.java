package com.example.demo.location.service;

import com.example.demo.location.domain.Location;
import com.example.demo.location.domain.LocationPostList;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface LocationService {
    List<Location> selectLocationList();

    Location selectLocationByPost(int postNum);

    Location selectLocationDetail(int locationNum);

    List<LocationPostList> selectPostListByLocation(int locationNum);

    int selectLocationCount(String value);

    int selectLocationNumByValue(String value);

    void insertLocation(Location location);

    void insertPostLocation(PostSaveFacadeDomain postSaveFacadeDomain);
}
