package com.example.demo.location.service;

import com.example.demo.location.domain.Location;
import com.example.demo.location.domain.LocationPostList;
import com.example.demo.location.persistence.LocationMapper;
import com.example.demo.photo.persistence.PhotoMapper;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LocationServiceImpl implements LocationService {

    private final LocationMapper locationMapper;
    private final PhotoMapper photoMapper;

    public List<Location> selectLocationList() {
        return locationMapper.selectLocationList();
    }

    public Location selectLocationByPost(int postNum) {
        Location location = locationMapper.selectLocationByPost(postNum);
        location.setHrefUrl(location.getNum());

        return location;
    }

    public Location selectLocationDetail(int locationNum) {
        return locationMapper.selectLocationDetail(locationNum);
    }

    public List<LocationPostList> selectPostListByLocation(int locationNum) {
        List<LocationPostList> locationPostLists = locationMapper.selectPostListByLocation(locationNum);

        locationPostLists.stream().forEach(locationPostList -> {
            int postNum = locationPostList.getPostNum();

            locationPostList.setMainPostPhoto(photoMapper.selectLastPhotoByPost(postNum));
            locationPostList.setPhotoCount(photoMapper.selectPhotoCountByPost(postNum));
        });

        return locationPostLists;
    }

    public int selectLocationCount(String value) {
        return locationMapper.selectLocationCount(value);
    }

    public int selectLocationNumByValue(String value) {
        return locationMapper.selectLocationNumByValue(value);
    }

    public void insertLocation(Location location) {
        locationMapper.insertLocation(location);
    }

    public void insertPostLocation(PostSaveFacadeDomain postSaveFacadeDomain) {
        locationMapper.insertPostLocation(postSaveFacadeDomain);
    }
}
