package com.example.demo.location.domain;

import com.example.demo.photo.domain.Photo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@Component
public class LocationPostList {
    private int rowNum;
    private int postNum;
    private int photoCount;
    private Photo mainPostPhoto;
}
