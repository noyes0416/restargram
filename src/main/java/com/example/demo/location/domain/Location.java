package com.example.demo.location.domain;

import com.example.demo.common.domain.AdditionalInfo;
import org.springframework.stereotype.Component;

import static com.example.demo.common.domain.Constant.LOCATION_LIST_PATH;

@Component
public class Location extends AdditionalInfo {
    private String hrefUrl;

    public String getHrefUrl() {
        return this.hrefUrl;
    }

    public void setHrefUrl(int locationNum) {
        StringBuilder sb = new StringBuilder();

        sb.append(LOCATION_LIST_PATH);
        sb.append(locationNum);

        this.hrefUrl = sb.toString();
    }
}
