package com.example.demo.location.persistence;

import com.example.demo.location.domain.LocationPostList;
import com.example.demo.location.domain.Location;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LocationMapper {
    List<Location> selectLocationList();

    Location selectLocationByPost(int postNum);

    Location selectLocationDetail(int locationNum);

    List<LocationPostList> selectPostListByLocation(int locationNum);

    int selectLocationCount(String value);

    int selectLocationNumByValue(String value);

    void insertLocation(Location location);

    void insertPostLocation(PostSaveFacadeDomain postSaveFacadeDomain);
}
