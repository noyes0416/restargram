package com.example.demo.location.controller;

import com.example.demo.location.domain.LocationPostList;
import com.example.demo.location.service.LocationService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.reply.domain.Reply;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.example.demo.common.domain.Constant.LOCATION_POSTLIST_VIEW_PATH;

@RestController
@RequiredArgsConstructor
@RequestMapping("/Location")
public class LocationController {

    private final LocationService locationService;

    @RequestMapping(value = "/PostList", method = RequestMethod.GET)
    public ModelAndView PostListByLocation(int locationNum) {
        ModelAndView mav = new ModelAndView(LOCATION_POSTLIST_VIEW_PATH);

        List<LocationPostList> locationPostLists = locationService.selectPostListByLocation(locationNum);

        mav.addObject("reply", new Reply());
        mav.addObject("postSaveFacadeDomain", new PostSaveFacadeDomain());
        mav.addObject("location", locationService.selectLocationDetail(locationNum));
        mav.addObject("topPostPhoto", locationPostLists.get(0));
        mav.addObject("postListByLocation", locationPostLists);

        return mav;
    }
}
