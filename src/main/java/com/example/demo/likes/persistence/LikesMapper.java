package com.example.demo.likes.persistence;

import com.example.demo.likes.domain.Likes;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LikesMapper {
    void insertLikes(Likes likes);
    void deleteLikes(Likes likes);
    int selectLikesCountByPost(int likes);
}
