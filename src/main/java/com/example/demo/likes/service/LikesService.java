package com.example.demo.likes.service;

import com.example.demo.likes.domain.Likes;
import org.springframework.stereotype.Service;

@Service
public interface LikesService {
    int insertLikes(Likes likes);
    void deleteLikes(Likes likes);
    int selectLikesCountByPost(int postNum);
}
