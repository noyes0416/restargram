package com.example.demo.likes.service;

import com.example.demo.likes.domain.Likes;
import com.example.demo.likes.persistence.LikesMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class LikesServiceImpl implements LikesService{

    private final LikesMapper likesMapper;

    @Transactional
    public int insertLikes(Likes likes) {
        likesMapper.insertLikes(likes);
        return likesMapper.selectLikesCountByPost(likes.getTypeValue());
    }

    public void deleteLikes(Likes likes) {
        likesMapper.deleteLikes(likes);
    }

    public int selectLikesCountByPost(int postNum) {
        return likesMapper.selectLikesCountByPost(postNum);
    }
}
