package com.example.demo.likes.domain;

import com.example.demo.common.enumeration.LikeTypeEnum;
import org.springframework.stereotype.Component;

@Component
public class Likes {
    private int num;
    private int userNum;
    private LikeTypeEnum likeType;
    private int typeValue;
    private int likesCountByPost;

    public int getNum() {
        return this.num;
    }

    public int getUserNum() {
        return this.userNum;
    }

    public LikeTypeEnum getLikeType() {
        return this.likeType;
    }

    public int getTypeValue() {
        return this.typeValue;
    }

    public int getLikesCountByPost() { return this.likesCountByPost; }

    public void setNum(int num) {
        this.num = num;
    }

    public void setUserNum(int userNum) { this.userNum = userNum; }

    public void setLikeType(LikeTypeEnum likeType) { this.likeType = likeType; }

    public void setTypeValue(int typeValue) {
        this.typeValue = typeValue;
    }

    public void setLikesCountByPost(int count) { this.likesCountByPost = count; }
}
