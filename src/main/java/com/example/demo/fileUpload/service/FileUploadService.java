package com.example.demo.fileUpload.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.example.demo.common.enumeration.PhotoTypeEnum;
import com.example.demo.photo.domain.Photo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class FileUploadService {

    private final UploadService s3Service;

    public Photo uploadImage(MultipartFile file, PhotoTypeEnum photoTypeEnum) {
        Photo photo = new Photo();
        String originFileName = file.getOriginalFilename();
        String fileName = createFileName(originFileName);

        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(file.getSize());
        objectMetadata.setContentType(file.getContentType());

        try(InputStream inputStream = file.getInputStream()) {
            s3Service.uploadFile(inputStream, objectMetadata, fileName);

            photo.setFilePath(s3Service.getFileUrl(fileName));
        }
        catch (IOException e) {
            throw new IllegalArgumentException(String.format("Error Occured during file transfer (%s)", file.getOriginalFilename()));
        }

        photo.setPhotoType(photoTypeEnum);
        photo.setOriginName(originFileName);
        photo.setFileName(fileName);

        return photo;
    }

    private String createFileName(String originalFileName) {
        return UUID.randomUUID().toString().concat(getFileExtension(originalFileName));
    }

    private String getFileExtension(String fileName) {
        try{
            return fileName.substring((fileName.lastIndexOf(".")));
        }
        catch(StringIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(String.format("Wrong file of format (%s)", fileName));
        }
    }
}
