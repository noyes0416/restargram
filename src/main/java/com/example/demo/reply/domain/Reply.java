package com.example.demo.reply.domain;

import com.example.demo.common.enumeration.LikeTypeEnum;
import com.example.demo.likes.domain.Likes;
import com.example.demo.common.domain.Writing;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class Reply extends Writing {

    private final LikeTypeEnum LIKES_LOCATION_REPLY = LikeTypeEnum.REPLY;

    private int parentReplyNum;
    private List<Reply> secondDepthReplyList;
    private int postNum;
    private int replyCountByPost;
    private boolean replyLikedFlagByUser;
    private LocalDateTime updDt;
    private String profilePhotoFilePath;

    public int getParentReplyNum() { return this.parentReplyNum; }

    public List<Reply> getSecondDepthReplyList() {
        return this.secondDepthReplyList;
    }

    public int getPostNum() {
        return this.postNum;
    }

    public boolean getReplyLikedFlagByUser() {
        return this.replyLikedFlagByUser;
    }

    public String getUpdDt() {
        return Optional.ofNullable(this.updDt).isPresent() ? this.updDt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) : "";
    }

    public String getProfilePhotoFilePath() {
        return this.profilePhotoFilePath;
    }

    public void setParentReplyNum(int parentReplyNum) {
        this.parentReplyNum = parentReplyNum;
    }

    public void setSecondDepthReplyList(List<Reply> secondDepthReplyList) {
        this.secondDepthReplyList = secondDepthReplyList;
    }

    public void setPostNum(int postNum) {
        this.postNum = postNum;
    }

    public void setReplyCountByPost(int count) { this.replyCountByPost = count; }

    public void setDefaultUpdDt() {
        this.updDt = LocalDateTime.now();
    }

    public void setProfilePhotoFilePath(String profilePhotoFilePath) {
        this.profilePhotoFilePath = profilePhotoFilePath;
    }

    public int getReplyCountByPost() {
        return this.replyCountByPost;
    }

    public Likes getLikesFromReply(int userNum, int replyNum) {
        Likes likes = new Likes();
        likes.setUserNum(userNum);
        likes.setLikeType(LIKES_LOCATION_REPLY);
        likes.setTypeValue(replyNum);

        return likes;
    }

    public void setSecondReplyContent() {
        String removeUserIdContent = (Arrays.asList(this.getContent().split(" ")).stream()
                .skip(1)
                .collect(Collectors.joining(" ")));

        this.setContent(removeUserIdContent);
    }
}
