package com.example.demo.reply.controller;

import com.example.demo.likes.service.LikesService;
import com.example.demo.reply.domain.Reply;
import com.example.demo.reply.service.ReplyService;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.Optional;

import static com.example.demo.common.domain.Constant.*;

@RequestMapping("/Reply")
@RequiredArgsConstructor
@Controller
public class ReplyController {

    private final LikesService likesService;
    private final ReplyService replyService;

    @RequestMapping(value = "/InsertLikes", method = RequestMethod.GET)
    public String InsertReplyLikes(HttpServletRequest request, int postNum, int replyNum) {
        UserPhoto sessionUserInfo = (UserPhoto) request.getSession().getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        likesService.insertLikes(new Reply().getLikesFromReply(userNum, replyNum));

        return POST_LIST_REDIRECT_PATH;
    }

    @RequestMapping(value = "/DeleteLikes", method = RequestMethod.GET)
    public String DeleteReplyByPost(HttpServletRequest request, int postNum, int replyNum) {
        UserPhoto sessionUserInfo = (UserPhoto) request.getSession().getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        likesService.deleteLikes(new Reply().getLikesFromReply(userNum, replyNum));

        return POST_LIST_REDIRECT_PATH;
    }

    @RequestMapping(value = "/Save", method = RequestMethod.POST)
    public String ReplySave(@RequestParam("userNum") int userNum, @ModelAttribute("reply") Reply reply) throws IOException {
        if (Optional.ofNullable(reply.getParentReplyNum()).orElse(0) != 0) {
            reply.setSecondReplyContent();
        }

        replyService.insertReply(reply);

        return POST_LIST_REDIRECT_PATH;
    }
}
