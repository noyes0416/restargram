package com.example.demo.reply.persistence;

import com.example.demo.reply.domain.Reply;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReplyMapper {
    List<Reply> selectReplyList(Map hashMap);

    List<Reply> selectSecondDepthReplyList(Map hashMap);

    void insertReply(Reply reply);

    void updateReply(Reply reply);

    int selectReplyCountByPost(int postNum);
}
