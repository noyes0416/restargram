package com.example.demo.reply.service;

import com.example.demo.reply.domain.Reply;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface ReplyService {
    List<Reply> selectReplyList(Map hashMap);

    List<Reply> selectSecondDepthReplyList(Map hashMap);

    void insertReply(Reply reply);

    void updateReply(Reply reply);

    int selectReplyCountByPost(int postNum);
}
