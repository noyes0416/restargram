package com.example.demo.reply.service;

import com.example.demo.hashtag.persistence.HashtagMapper;
import com.example.demo.reply.domain.Reply;
import com.example.demo.reply.persistence.ReplyMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class ReplyServiceImpl implements ReplyService {

    private final ReplyMapper replyMapper;
    private final HashtagMapper hashtagMapper;

    public List<Reply> selectReplyList(Map hashMap) {
        return replyMapper.selectReplyList(hashMap);
    }

    public List<Reply> selectSecondDepthReplyList(Map hashMap) {
        return replyMapper.selectSecondDepthReplyList(hashMap);
    }

    public void insertReply(Reply reply) {
        String replyContent = reply.getContent();
        reply.setContentFromString(replyContent);
        reply.setHashtagListFromString(replyContent);

        replyMapper.insertReply(reply);
        insertReplyHashtag(reply);
    }

    private void insertReplyHashtag(Reply reply) {
        int replyNum = reply.getNum();

        reply.getHashtagList().forEach(hashtag -> {
            int hashtagValueCount = hashtagMapper.selectHashtagCount(hashtag.getValue());
            Map map = new HashMap<>();
            map.put("replyNum", replyNum);

            if (hashtagValueCount > 0) {
                map.put("hashtagNum", hashtagMapper.selectHashtagNumByValue(hashtag.getValue()));
            }
            else {
                hashtagMapper.insertHashtag(hashtag);
                map.put("hashtagNum", hashtag.getNum());
            }

            hashtagMapper.insertReplyHashtag(map);
        });
    }

    public void updateReply(Reply reply) {
        replyMapper.updateReply(reply);
    }

    public int selectReplyCountByPost(int postNum) {
        return replyMapper.selectReplyCountByPost(postNum);
    }
}
