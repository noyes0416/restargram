package com.example.demo.photo.domain;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.common.enumeration.PhotoTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Setter
@Getter
@Component
public class Photo {

    private int rowNum;
    private String eachPhotoGridViewSize;

    private int num;
    private String originName;
    private String fileName;
    private String filePath;
    private PhotoTypeEnum photoType;
    private ActiveStateEnum state;
    private LocalDateTime regDt;
}
