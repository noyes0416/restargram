package com.example.demo.photo.service;

import com.example.demo.common.enumeration.PhotoTypeEnum;
import com.example.demo.fileUpload.service.FileUploadService;
import com.example.demo.photo.domain.Photo;
import com.example.demo.photo.persistence.PhotoMapper;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.user.domain.UserPhoto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

import static com.example.demo.common.domain.Constant.POST_PHOTO_VIEW_BASIC_GRID;

@RequiredArgsConstructor
@Service
public class PhotoServiceImpl implements PhotoService {

    private final PhotoMapper photoMapper;
    private final FileUploadService fileUploadService;

    public List<Photo> selectPhotoByPost(int postNum) {
        List<Photo> photoList = photoMapper.selectPhotoByPost(postNum);

        photoList.stream().forEach(photo -> {
            photo.setEachPhotoGridViewSize(String.format("%dpx", (photo.getRowNum() - 1) * POST_PHOTO_VIEW_BASIC_GRID));
        });

        return photoList;
    }

    public int selectPhotoCountByPost(int postNum) {
        return photoMapper.selectPhotoCountByPost(postNum);
    }

    public Photo selectPhotoByUser(int userNum) {
        return photoMapper.selectPhotoByUser(userNum);
    }

    public void insertPostPhoto(PostSaveFacadeDomain postSaveFacadeDomain) throws IOException {
        postSaveFacadeDomain.getMultipartFileList().forEach(multipartFile -> {
            postSaveFacadeDomain.setPhoto(fileUploadService.uploadImage(multipartFile, PhotoTypeEnum.PHOTO));

            photoMapper.insertPhoto(postSaveFacadeDomain.getPhoto());
            photoMapper.insertPostPhoto(postSaveFacadeDomain);
        });
    }

    public void insertUserPhoto(UserPhoto userPhoto) throws IOException {
        userPhoto.setPhoto(fileUploadService.uploadImage(userPhoto.getMultipartFile(), PhotoTypeEnum.USER));

        photoMapper.updatePhoto(userPhoto.getUser().getNum());
        photoMapper.insertPhoto(userPhoto.getPhoto());
        photoMapper.insertUserPhoto(userPhoto);
    }
}
