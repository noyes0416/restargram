package com.example.demo.photo.service;

import com.example.demo.photo.domain.Photo;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.user.domain.UserPhoto;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public interface PhotoService {
    List<Photo> selectPhotoByPost(int postNum);

    int selectPhotoCountByPost(int postNum);

    Photo selectPhotoByUser(int userNum);

    void insertPostPhoto(PostSaveFacadeDomain postSaveFacadeDomain) throws IOException;

    void insertUserPhoto(UserPhoto userPhoto) throws IOException;
}
