package com.example.demo.photo.persistence;

import com.example.demo.photo.domain.Photo;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.user.domain.UserPhoto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PhotoMapper {
    List<Photo> selectPhotoByPost(int postNum);

    Photo selectLastPhotoByPost(int postNum);

    int selectPhotoCountByPost(int postNum);

    Photo selectPhotoByUser(int userNum);

    void insertPhoto(Photo photo);

    void insertPostPhoto(PostSaveFacadeDomain postSaveFacadeDomain);

    void insertUserPhoto(UserPhoto userPhoto);

    void updatePhoto(int userNum);
}
