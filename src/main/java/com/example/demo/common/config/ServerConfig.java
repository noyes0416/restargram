package com.example.demo.common.config;

import com.example.demo.common.interceptor.AuthInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;
import java.util.List;

@Configuration
public class ServerConfig implements WebMvcConfigurer {
    private static final List<String> INACCESSIBLE_URL_PATTERNS = Arrays.asList("/Post/*", "/User/*", "/Message/*");
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AuthInterceptor()).addPathPatterns(INACCESSIBLE_URL_PATTERNS);
    }
}
