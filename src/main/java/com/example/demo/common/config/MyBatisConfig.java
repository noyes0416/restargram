package com.example.demo.common.config;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.common.enumeration.LikeTypeEnum;
import com.example.demo.common.enumeration.PhotoTypeEnum;
import com.example.demo.common.enumeration.UserStateEnum;
import org.apache.ibatis.type.BooleanTypeHandler;
import org.apache.ibatis.type.TypeHandler;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.io.IOException;

@Configuration
@MapperScan("com.example.demo.*.persistence")
public class MyBatisConfig {

    @Bean
    public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource) throws IOException {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setTypeAliasesPackage("com.example.demo.*.domain");
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath*:mapper/**/*.xml"));
        sessionFactory.setTypeHandlers(new TypeHandler[]
                {
                        new BooleanTypeHandler(),
                        new ActiveStateEnum.TypeHandler(),
                        new LikeTypeEnum.TypeHandler(),
                        new UserStateEnum.TypeHandler(),
                        new PhotoTypeEnum.TypeHandler()
                });
        return sessionFactory;
    }
}
