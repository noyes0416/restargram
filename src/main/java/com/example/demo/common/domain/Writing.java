package com.example.demo.common.domain;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.common.utils.Utils;
import com.example.demo.hashtag.domain.Hashtag;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public abstract class Writing {
    private int num;
    private int userNum;
    private String userId;
    private String content;
    private List<Hashtag> hashtagList;
    private ActiveStateEnum state;
    private LocalDateTime regDt;

    public int getNum() {
        return this.num;
    }

    public int getUserNum() {
        return this.userNum;
    }

    public String getUserId() {
        return this.userId;
    }

    public String getContent() {
        return this.content;
    }

    public List<Hashtag> getHashtagList() {
        return this.hashtagList;
    }

    public ActiveStateEnum getState() { return this.state; }

    public String getRegDt() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return this.regDt.format(formatter);
    }

    public void setNum (int num) {
        this.num = num;
    }

    public void setUserNum(int userNum) {
        this.userNum = userNum;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setContentFromString(String string) {
        this.content = Utils.SplitContentFromString(string);
    }

    public void setState(ActiveStateEnum state) { this.state = state; }

    public void setHashtagList(List<Hashtag> hashtagList) {
        this.hashtagList = hashtagList;

        ArrayList<String> hrefHashtagArray =  new ArrayList<>();

        hashtagList.forEach(hashtag -> {
            hashtag.setHrefUrl(hashtag.getNum());

            String hrefHashtag = String.format("<a class=\' xil3i\' href=\'%s\' tabindex=\'0\'>%s</a>"
                ,hashtag.getHrefUrl()
                ,hashtag.getValue());

            hrefHashtagArray.add(hrefHashtag);
        });

        this.setContent(String.format(this.getContent(), hrefHashtagArray.toArray()));
    }

    public void setHashtagListFromString(String string) {
        this.hashtagList = Utils.SplitHashtagListFromString(string);
    }
}
