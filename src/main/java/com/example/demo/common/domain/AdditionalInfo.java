package com.example.demo.common.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class AdditionalInfo {
    private int num;
    private String value = "";
    private LocalDateTime regDt;

    public int getNum() { return this.num; }

    public String getValue() {
        return this.value;
    }

    public String getRegDt() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return this.regDt.format(formatter);
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
