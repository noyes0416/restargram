package com.example.demo.common.domain;

public abstract class Social {
    private int num;
    private int sender;
    private int receiver;

    public int getNum() { return this.num; }
    public int getSender() { return this.sender; }
    public int getReceiver() { return this.receiver; }

    public void setSender(int sender){this.sender = sender;}
    public void setReceiver(int receiver){this.receiver = receiver;}
}