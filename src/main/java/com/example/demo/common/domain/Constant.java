package com.example.demo.common.domain;

public final class Constant {
    public static int POST_PHOTO_VIEW_BASIC_GRID = 614;

    public static String SIGNIN_VIEW_PATH = "signIn";
    public static String SIGNUP_VIEW_PATH = "signUp";

    public static String USER_RECOMMENDED_VIEW_PATH = "user/recommended";
    public static String USER_EDIT_VIEW_PATH = "user/edit";
    public static String USER_PROFILE_VIEW_PATH = "user/profile";

    public static String POST_LIST_VIEW_PATH = "post/list";

    public static String HASHTAG_POSTLIST_VIEW_PATH = "hashtag/postList";
    public static String HASHTAG_LIST_PATH = "/Hashtag/PostList?hashtagNum=";

    public static String LOCATION_POSTLIST_VIEW_PATH = "location/postList";
    public static String LOCATION_LIST_PATH = "/Location/PostList?locationNum=";

    public static String MESSAGE_VIEW_PATH = "message/message";

    //fragment view path
    public static String POST_DETAIL_MODAL_VIEW_PATH = "post/detail::detailModal";

    //redirect path
    public static String SIGNIN_VIEW_REDIRECT_PATH = "redirect:SignIn";
    public static String USER_RECOMMENDED_REDIRECT_PATH = "redirect:/User/Recommended";
    public static String POST_LIST_REDIRECT_PATH = "redirect:/Post/List";
    public static String MESSAGE_LIST_REDIRECT_PATH = "redirect:/Message/List?id=%s&groupNum=%s";
}
