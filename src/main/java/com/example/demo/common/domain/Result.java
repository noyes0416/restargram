package com.example.demo.common.domain;

public class Result {
    private int code;
    private String message;

    public int getCode(){ return this.code; }
    public String getMessage() { return this.message; }

    public void setCode(int code){ this.code = code; }
    public void setMessage(String message){ this.message = message; }
}
