package com.example.demo.common.utils;

import com.example.demo.hashtag.domain.Hashtag;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {

    public static String SplitContentFromString(String content) {
        return (Arrays.asList(content.split(" ")).stream()
                .map(word -> {
                    if (word.contains("#")) {
                        word = "%s";
                    }
                    return word;
                })
                .collect(Collectors.joining(" ")));
    }

    public static List<Hashtag> SplitHashtagListFromString(String content) {
        return Arrays.asList(content.split(" ")).stream()
                .filter(each -> each.contains("#"))
                .distinct()
                .map(each -> {
                    Hashtag hashtag = new Hashtag();
                    hashtag.setValue(each.replace("#", ""));
                    return hashtag;
                })
                .collect(Collectors.toList());
    }
}
