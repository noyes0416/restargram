package com.example.demo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum UserStateEnum implements CodeEnum {
    ACTIVE(1),
    INACTIVE(2);

    private final int stateCode;

    UserStateEnum(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(UserStateEnum.class)
    public static class TypeHandler extends CodeEnumTypeHandler<UserStateEnum> {
        public TypeHandler() {
            super(UserStateEnum.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
