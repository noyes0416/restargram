package com.example.demo.common.enumeration;

public interface CodeEnum {
    int getCode();
}
