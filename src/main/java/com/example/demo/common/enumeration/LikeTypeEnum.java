package com.example.demo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum LikeTypeEnum implements CodeEnum {
    POST(1),
    REPLY(2),
    DM(3);

    private final int likeTypeCode;

    LikeTypeEnum(int likeTypeCode) {
        this.likeTypeCode = likeTypeCode;
    }

    @MappedTypes(LikeTypeEnum.class)
    public static class TypeHandler extends CodeEnumTypeHandler<LikeTypeEnum> {
        public TypeHandler() {
            super(LikeTypeEnum.class);
        }
    }

    @Override
    public int getCode() {
        return this.likeTypeCode;
    }
}
