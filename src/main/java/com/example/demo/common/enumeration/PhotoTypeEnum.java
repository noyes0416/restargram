package com.example.demo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum PhotoTypeEnum implements CodeEnum {
    USER(1),
    PHOTO(2);

    private final int likeTypeCode;

    PhotoTypeEnum(int likeTypeCode) {
        this.likeTypeCode = likeTypeCode;
    }

    @MappedTypes(PhotoTypeEnum.class)
    public static class TypeHandler extends CodeEnumTypeHandler<PhotoTypeEnum> {
        public TypeHandler() {
            super(PhotoTypeEnum.class);
        }
    }

    @Override
    public int getCode() {
        return this.likeTypeCode;
    }
}
