package com.example.demo.common.enumeration;

import org.apache.ibatis.type.MappedTypes;

public enum ActiveStateEnum implements CodeEnum {
    ACTIVE(1),
    INACTIVE(2);

    private final int stateCode;

    ActiveStateEnum(int stateCode) {
        this.stateCode = stateCode;
    }

    @MappedTypes(ActiveStateEnum.class)
    public static class TypeHandler extends CodeEnumTypeHandler<ActiveStateEnum> {
        public TypeHandler() {
            super(ActiveStateEnum.class);
        }
    }

    @Override
    public int getCode() {
        return this.stateCode;
    }
}
