package com.example.demo.message.persistence;

import com.example.demo.message.domain.Message;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MessageMapper {
    List<Message> selectGroupList(int num);
    void insertMessage(Message message);
    List<Message> selectMessageList(int groupNum);
    void insertContent(Message message);
    void insertGroup(int sender, int receiver);
    int selectGroupNum(int sender, int receiver);
    int isExistGroup(int sender, int receiver);
    void updateViewDt(int groupNum, int receiver);
}
