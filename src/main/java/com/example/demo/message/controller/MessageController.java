package com.example.demo.message.controller;

import com.example.demo.message.domain.Message;
import com.example.demo.message.service.MessageService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.example.demo.common.domain.Constant.*;

@Controller
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final UserService userService;
    private final PhotoService photoService;

    private final String DEFAULT_MESSAGE_PATH = "/Message";
    private final String DEFAULT_MESSAGE_VIEW_PATH = "/message";

    @GetMapping("/Message/Home")
    public String Home(Model model, HttpServletRequest httpServletRequest){
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();

        model.addAttribute("GroupInfos", messageService.selectGroupInfo(userNum));
        model.addAttribute("RecommendInfos", userService.selectRecommendList(userNum));
        model.addAttribute("postSaveFacadeDomain", new PostSaveFacadeDomain());

        return MESSAGE_VIEW_PATH;
    }

    @GetMapping("/Message/List")
    public String List(Model model, HttpServletRequest httpServletRequest, String id, int groupNum){
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        int receiverNum = userService.selectUserNum(id);

        UserPhoto userPhoto = new UserPhoto();
        userPhoto.setUser(userService.selectUserInfo(receiverNum));
        userPhoto.setPhoto(photoService.selectPhotoByUser(userPhoto.getUser().getNum()));

        model.addAttribute("GroupInfos", messageService.selectGroupInfo(userNum));
        model.addAttribute("LoadMessages", messageService.selectMessageList(groupNum, userNum));
        model.addAttribute("receiverInfo", userPhoto);
        model.addAttribute("groupNum", groupNum);
        model.addAttribute("RecommendInfos", userService.selectRecommendList(userNum));

        return MESSAGE_VIEW_PATH;
    }

    @PostMapping("/Message/Send")
    public String Send(@Valid Message message, Model model, HttpServletRequest httpServletRequest){
        {
            String receiverId = userService.selectUserInfo(message.getReceiver()).getId();

            messageService.insertMessage(message);

            return String.format(MESSAGE_LIST_REDIRECT_PATH, receiverId, message.getGroupNum());
        }
    }

    @GetMapping("/Message/Create")
    public String CreateGroup(Model model, HttpServletRequest httpServletRequest, String id){
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        int receiverNum = userService.selectUserNum(id);
        int groupNum = messageService.insertGroup(userNum, receiverNum);

        return String.format(MESSAGE_LIST_REDIRECT_PATH, id, groupNum);
    }

    @PostMapping(DEFAULT_MESSAGE_PATH + "/SendPost")
    public String SendPost(@Valid Message message, Model model, HttpServletRequest httpServletRequest){
        {
            String receiverId = userService.selectUserInfo(message.getReceiver()).getId();

            int groupNum = messageService.insertPostMessage(message);

            return String.format(MESSAGE_LIST_REDIRECT_PATH, receiverId, groupNum);
        }
    }
}
