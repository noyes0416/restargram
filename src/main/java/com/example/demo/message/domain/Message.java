package com.example.demo.message.domain;

import com.example.demo.common.domain.Social;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class Message extends Social {
    private int groupNum;
    private int postNum = 0;
    private String content;
    private LocalDateTime regDt;
    private LocalDateTime viewDt;

    public int getGroupNum(){ return this.groupNum; }
    public int getPostNum(){ return this.postNum; }
    public String getContent(){ return this.content; }
    public String getViewDt()
    {
        if(this.viewDt == null){
            return null;
        }
        else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return this.viewDt.format(formatter);
        }
    }
    public String getRegDt()
    {
        if(this.regDt == null){
            return null;
        }
        else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return this.regDt.format(formatter);
        }
    }

    public void setGroupNum(int groupNum){ this.groupNum = groupNum; }
    public void setPostNum(int postNum){ this.postNum = postNum; }
    public void setContent(String content) { this.content = content; }
}
