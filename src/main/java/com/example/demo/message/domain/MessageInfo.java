package com.example.demo.message.domain;

import com.example.demo.post.domain.PostListFacadeDomain;
import com.example.demo.user.domain.UserPhoto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Builder
@Component
public class MessageInfo {
    private Message message;
    private UserPhoto userPhoto;
    private PostListFacadeDomain postListFacadeDomain;
}
