package com.example.demo.message.service.Impl;

import com.example.demo.common.domain.Result;
import com.example.demo.likes.domain.Likes;
import com.example.demo.message.domain.Message;
import com.example.demo.message.domain.MessageInfo;
import com.example.demo.message.persistence.MessageMapper;
import com.example.demo.message.service.MessageService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.Post;
import com.example.demo.post.domain.PostListFacadeDomain;
import com.example.demo.post.facade.PostListFacade;
import com.example.demo.reply.domain.Reply;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {
    private final MessageMapper mapper;
    private final UserService userService;
    private final PhotoService photoService;
    private final PostListFacade postListFacade;

    private List<MessageInfo> messageInfoList;
    private List<Message> messageList;

    public List<MessageInfo> selectGroupInfo(int num){

        messageInfoList = new ArrayList<>();

        messageList = selectGroupList(num);


        messageList.forEach(message -> {
            UserPhoto userPhoto = new UserPhoto();

            int sender = message.getSender();
            int receiver = message.getReceiver();
            int someone = sender == num ? receiver:sender;

            userPhoto.setUser(userService.selectUserInfo(someone));
            userPhoto.setPhoto(photoService.selectPhotoByUser(someone));

            MessageInfo messageInfo = MessageInfo.builder()
                    .message(message)
                    .userPhoto(userPhoto)
                    .build();

            messageInfoList.add(messageInfo);
        });

        return messageInfoList;

    }

    public List<Message> selectGroupList(int num){
        return mapper.selectGroupList(num);
    }

//    public List<Message> selectMessageList(int groupNum, int receiver)
//    {
//        mapper.updateViewDt(groupNum, receiver);
//        return mapper.selectMessageList(groupNum);
//    }

    public List<MessageInfo> selectMessageList(int groupNum, int receiver){
        messageInfoList = new ArrayList<>();

        messageList = mapper.selectMessageList(groupNum);
        mapper.updateViewDt(groupNum, receiver);

        messageList.forEach(message -> {
            UserPhoto userPhoto = new UserPhoto();

            int sender = message.getSender();

            userPhoto.setUser(userService.selectUserInfo(receiver));
            userPhoto.setPhoto(photoService.selectPhotoByUser(receiver));

            MessageInfo messageInfo = MessageInfo.builder()
                    .message(message)
                    .userPhoto(userPhoto)
                    .postListFacadeDomain(postListFacade.selectPostFacade(message.getPostNum()))
                    .build();

            messageInfoList.add(messageInfo);
        });

        return messageInfoList;
    }


    public void insertMessage(Message message)
    {
        mapper.insertMessage(message);
    }

    public int insertPostMessage(Message message)
    {
        int groupNum = insertGroup(message.getSender(), message.getReceiver());

        message.setGroupNum(groupNum);

        insertMessage(message);

        return groupNum;
    }

    public int insertGroup(int sender, int receiver){
        int groupExist = mapper.isExistGroup(sender, receiver);

        if (groupExist == 0)
        {
            mapper.insertGroup(sender, receiver);
        }

        int groupNum = mapper.selectGroupNum(sender, receiver);

        return groupNum;
    }
}
