package com.example.demo.message.service;

import com.example.demo.common.domain.Result;
import com.example.demo.message.domain.Message;
import com.example.demo.message.domain.MessageInfo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MessageService {
    List<MessageInfo> selectGroupInfo(int num);
    void insertMessage(Message message);
    List<MessageInfo> selectMessageList(int groupNum, int receiver);
    int insertGroup(int sender, int receiver);
    int insertPostMessage(Message message);
}
