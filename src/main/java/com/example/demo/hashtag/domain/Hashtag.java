package com.example.demo.hashtag.domain;

import com.example.demo.common.domain.AdditionalInfo;
import org.springframework.stereotype.Component;

import static com.example.demo.common.domain.Constant.*;

@Component
public class Hashtag extends AdditionalInfo {

    private String hrefUrl;

    public String getHrefUrl() { return this.hrefUrl; }

    public void setHrefUrl(int hashtagNum) {
        StringBuilder sb = new StringBuilder();

        sb.append(HASHTAG_LIST_PATH);
        sb.append(hashtagNum);

        this.hrefUrl = sb.toString();
    }
}
