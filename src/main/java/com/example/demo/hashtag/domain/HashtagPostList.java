package com.example.demo.hashtag.domain;

import com.example.demo.photo.domain.Photo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@NoArgsConstructor
@Component
public class HashtagPostList {
    private int rowNum;
    private int postNum;
    private int photoCount;
    private Photo mainPostPhoto;
}
