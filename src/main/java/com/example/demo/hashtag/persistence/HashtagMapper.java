package com.example.demo.hashtag.persistence;

import com.example.demo.hashtag.domain.HashtagPostList;
import com.example.demo.hashtag.domain.Hashtag;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface HashtagMapper {
    List<Hashtag> selectHashtagList();

    List<Hashtag> selectHashtagByPost(int postNum);

    List<Hashtag> selectHashtagByReply(int replyNum);

    Hashtag selectHashtagDetail(int hashtagNum);

    List<HashtagPostList> selectPostListByHashtag(int hashtagNum);

    int selectPostCountByHashtag(int hashtagNum);

    int selectHashtagCount(String value);

    int selectHashtagNumByValue(String value);

    void insertHashtag(Hashtag hashtag);

    void insertPostHashtag(Map map);

    void insertReplyHashtag(Map map);
}
