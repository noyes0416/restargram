package com.example.demo.hashtag.service;

import com.example.demo.hashtag.domain.HashtagPostList;
import com.example.demo.hashtag.domain.Hashtag;
import com.example.demo.hashtag.persistence.HashtagMapper;
import com.example.demo.photo.persistence.PhotoMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class HashtagServiceImpl implements HashtagService {

    private final HashtagMapper hashtagMapper;
    private final PhotoMapper photoMapper;

    public List<Hashtag> selectHashtagList() {
        return hashtagMapper.selectHashtagList();
    }

    public List<Hashtag> selectHashtagByReply(int replyNum) {
        return hashtagMapper.selectHashtagByReply(replyNum);
    }

    public List<Hashtag> selectHashtagByPost(int postNum) {
        List<Hashtag> hashtagList = hashtagMapper.selectHashtagByPost(postNum);

        return hashtagList.stream()
                .map(hashtag -> {
                    hashtag.setHrefUrl(hashtag.getNum());
                    return hashtag;
                })
                .collect(Collectors.toList());
    }

    public Hashtag selectHashtagDetail(int hashtagNum) {
        return hashtagMapper.selectHashtagDetail(hashtagNum);
    }

    public List<HashtagPostList> selectPostListByHashtag(int hashtagNum) {
        List<HashtagPostList> hashtagPostLists = hashtagMapper.selectPostListByHashtag(hashtagNum);

        hashtagPostLists.stream().forEach(hashtagPostList -> {
            int postNum = hashtagPostList.getPostNum();

            hashtagPostList.setMainPostPhoto(photoMapper.selectLastPhotoByPost(postNum));
            hashtagPostList.setPhotoCount(photoMapper.selectPhotoCountByPost(postNum));
        });

        return hashtagPostLists;
    }

    public int selectPostCountByHashtag(int hashtagNum) {
        return hashtagMapper.selectPostCountByHashtag(hashtagNum);
    }

    public int selectHashtagCount(String value) {
        return hashtagMapper.selectHashtagCount(value);
    }

    public int selectHashtagNumByValue(String value) {
        return hashtagMapper.selectHashtagNumByValue(value);
    }

    public void insertHashtag(Hashtag hashtag) {
        hashtagMapper.insertHashtag(hashtag);
    }

    public void insertPostHashtag(Map map) {
        hashtagMapper.insertPostHashtag(map);
    }
}
