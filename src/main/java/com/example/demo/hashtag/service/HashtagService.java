package com.example.demo.hashtag.service;

import com.example.demo.hashtag.domain.HashtagPostList;
import com.example.demo.hashtag.domain.Hashtag;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface HashtagService {
    List<Hashtag> selectHashtagList();

    List<Hashtag> selectHashtagByPost(int postNum);

    List<Hashtag> selectHashtagByReply(int replyNum);

    Hashtag selectHashtagDetail(int hashtagNum);

    List<HashtagPostList> selectPostListByHashtag(int hashtagNum);

    int selectPostCountByHashtag(int hashtagNum);

    int selectHashtagCount(String value);

    int selectHashtagNumByValue(String value);

    void insertHashtag(Hashtag hashtag);

    void insertPostHashtag(Map map);
}
