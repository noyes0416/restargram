package com.example.demo.hashtag.controller;

import com.example.demo.hashtag.domain.HashtagPostList;
import com.example.demo.hashtag.service.HashtagService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.reply.domain.Reply;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import static com.example.demo.common.domain.Constant.*;

@Controller
@RequiredArgsConstructor
@RequestMapping(value = "/Hashtag")
public class HashtagController {

    private final HashtagService hashtagService;

    @RequestMapping(value = "/PostList", method = RequestMethod.GET)
    public ModelAndView PostListByHashtag(int hashtagNum) {
        ModelAndView mav = new ModelAndView(HASHTAG_POSTLIST_VIEW_PATH);

        List<HashtagPostList> hashtagPostLists = hashtagService.selectPostListByHashtag(hashtagNum);

        mav.addObject("reply", new Reply());
        mav.addObject("postSaveFacadeDomain", new PostSaveFacadeDomain());
        mav.addObject("hashtag", hashtagService.selectHashtagDetail(hashtagNum));
        mav.addObject("topPostPhoto", hashtagPostLists.get(0));
        mav.addObject("postListByHashtag", hashtagPostLists);

        return mav;
    }
}
