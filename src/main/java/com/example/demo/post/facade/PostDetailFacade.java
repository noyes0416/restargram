package com.example.demo.post.facade;

import com.example.demo.hashtag.service.HashtagService;
import com.example.demo.likes.domain.Likes;
import com.example.demo.likes.service.LikesService;
import com.example.demo.location.service.LocationService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.Post;
import com.example.demo.post.domain.PostDetailFacadeDomain;
import com.example.demo.post.service.PostService;
import com.example.demo.reply.domain.Reply;
import com.example.demo.reply.service.ReplyService;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class PostDetailFacade {

    private final UserService userService;
    private final PostService postService;
    private final PhotoService photoService;
    private final ReplyService replyService;
    private final LikesService likesService;
    private final LocationService locationService;
    private final HashtagService hashtagService;

    private PostDetailFacadeDomain postDetailFacadeDomain;

    private int userNum;
    private int postNum;
    private Map hashMap;

    public PostDetailFacadeDomain SelectPostDetailFacade(int userNum, int postNum) {
        setDefaultValue(userNum, postNum);

        postDetailFacadeDomain = PostDetailFacadeDomain.builder()
                .user(userService.selectUserByPost(postNum))
                .userPhoto(selectUserPhoto())
                .post(selectPostDetail())
                .photoList(photoService.selectPhotoByPost(postNum))
                .replyList(selectReplyListByPost())
                .likes(selectLikesCountByPost())
                .location(locationService.selectLocationByPost(postNum))
                .build();

        return postDetailFacadeDomain;
    }

    private void setDefaultValue(int userNum, int postNum) {
        this.userNum = userNum;
        this.postNum = postNum;

        hashMap = new HashMap<>();
        hashMap.put("userNum", userNum);
        hashMap.put("postNum", postNum);
    }

    private UserPhoto selectUserPhoto() {
        UserPhoto userPhoto = new UserPhoto();
        userPhoto.setPhoto(photoService.selectPhotoByUser(userNum));

        return userPhoto;
    }

    private Post selectPostDetail() {
        Post post = postService.selectPostDetail(hashMap);

        int postNum = post.getNum();
        int postPhotoCount = photoService.selectPhotoCountByPost(postNum);

        post.setPhotoCount(postPhotoCount);
        post.setPhotoViewGridSize(postPhotoCount);
        post.setHashtagList(hashtagService.selectHashtagByPost(postNum));

        return post;
    }

    private List<Reply> selectReplyListByPost() {
        List<Reply> replyList = replyService.selectReplyList(hashMap);

        replyList.forEach(reply -> {
            int replyNum = reply.getNum();
            reply.setHashtagList(hashtagService.selectHashtagByReply(replyNum));

            hashMap.put("replyNum", replyNum);

            reply.setSecondDepthReplyList(replyService.selectSecondDepthReplyList(hashMap));
            reply.getSecondDepthReplyList().forEach(secondDepthReply -> {
                int secondDepthReplyNum = secondDepthReply.getNum();
                secondDepthReply.setHashtagList(hashtagService.selectHashtagByReply(secondDepthReplyNum));
            });
        });

        return replyList;
    }

    private Likes selectLikesCountByPost() {
        Likes likes = new Likes();
        likes.setLikesCountByPost(likesService.selectLikesCountByPost(postNum));

        return likes;
    }
}
