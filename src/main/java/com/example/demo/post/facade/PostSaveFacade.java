package com.example.demo.post.facade;

import com.example.demo.hashtag.service.HashtagService;
import com.example.demo.location.service.LocationService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.post.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Component
@Transactional(rollbackFor=Exception.class)
public class PostSaveFacade {

    private final PostService postService;
    private final PhotoService photoService;
    private final LocationService locationService;
    private final HashtagService hashtagService;

    //TODO : Exception
    public void PostSave(PostSaveFacadeDomain postSaveFacadeDomain) throws IOException {
        SplitContentAndHashtag(postSaveFacadeDomain);
        DoPostSave(postSaveFacadeDomain);
        DoPhotoSave(postSaveFacadeDomain);
        DoLocationSave(postSaveFacadeDomain);
        DoHashtagSave(postSaveFacadeDomain);
    }

    private void SplitContentAndHashtag(PostSaveFacadeDomain postSaveFacadeDomain) {
        String postContent = postSaveFacadeDomain.getPost().getContent();
        postSaveFacadeDomain.getPost().setContentFromString(postContent);
        postSaveFacadeDomain.getPost().setHashtagListFromString(postContent);
    }

    private void DoPostSave(PostSaveFacadeDomain postSaveFacadeDomain) {
        postService.insertPost(postSaveFacadeDomain.getPost());
    }

    private void DoPhotoSave(PostSaveFacadeDomain postSaveFacaeDomain) throws IOException {
        photoService.insertPostPhoto(postSaveFacaeDomain);
    }

    private void DoLocationSave(PostSaveFacadeDomain postSaveFacadeDomain) {
        String locationValue = postSaveFacadeDomain.getLocation().getValue();
        int locationValueCount = locationService.selectLocationCount(locationValue);

        if (locationValueCount > 0) {
            postSaveFacadeDomain.getLocation().setNum(locationService.selectLocationNumByValue(locationValue));
        }
        else {
            locationService.insertLocation(postSaveFacadeDomain.getLocation());
        }

        locationService.insertPostLocation(postSaveFacadeDomain);
    }

    private void DoHashtagSave(PostSaveFacadeDomain postSaveFacadeDomain) {
        int postNum = postSaveFacadeDomain.getPost().getNum();

        postSaveFacadeDomain.getPost().getHashtagList().forEach(hashtag -> {
            int hashtagValueCount = hashtagService.selectHashtagCount(hashtag.getValue());
            Map map = new HashMap();
            map.put("postNum", postNum);

            if (hashtagValueCount > 0) {
                map.put("hashtagNum", hashtagService.selectHashtagNumByValue(hashtag.getValue()));
            }
            else {
                hashtagService.insertHashtag(hashtag);
                map.put("hashtagNum", hashtag.getNum());
            }

            hashtagService.insertPostHashtag(map);
        });
    }
}
