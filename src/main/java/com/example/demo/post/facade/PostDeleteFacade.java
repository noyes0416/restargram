package com.example.demo.post.facade;

import com.example.demo.post.domain.PostDeleteFacadeDomain;
import com.example.demo.post.service.PostService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class PostDeleteFacade {

    private PostDeleteFacadeDomain postDeleteFacadeDomain;

    private final PostService postService;

    public void PostDelete(int postNum) {
        postDeleteFacadeDomain = new PostDeleteFacadeDomain();
        postDeleteFacadeDomain.setPostNum(postNum);

        postService.deletePost(postDeleteFacadeDomain);
    }
}
