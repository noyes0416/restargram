package com.example.demo.post.facade;

import com.example.demo.photo.domain.Photo;
import com.example.demo.post.domain.PostListFacadeDomain;
import com.example.demo.hashtag.service.HashtagService;
import com.example.demo.likes.domain.Likes;
import com.example.demo.likes.service.LikesService;
import com.example.demo.location.service.LocationService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.Post;
import com.example.demo.post.service.PostService;
import com.example.demo.reply.domain.Reply;
import com.example.demo.reply.service.ReplyService;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component
public class PostListFacade {

    private final UserService userService;
    private final PostService postService;
    private final PhotoService photoService;
    private final ReplyService replyService;
    private final LikesService likesService;
    private final LocationService locationService;
    private final HashtagService hashtagService;

    private List<Post> postList;
    private List<PostListFacadeDomain> postListFacadeDomainList;
    private PostListFacadeDomain postListFacadeDomain;

    public List<PostListFacadeDomain> SelectAllPostListFacade(int userNum) {
        postList = postService.selectAllPostList(userNum);
        SelectPostListProcess();

        return postListFacadeDomainList;
    }

    public List<PostListFacadeDomain> SelectMyPostListFacade(int userNum) {
        postList = postService.selectMyPostList(userNum);
        SelectPostListProcess();

        return postListFacadeDomainList;
    }

    //TODO : 중복제거
    private void SelectPostListProcess() {
        postListFacadeDomainList = new ArrayList<>();

        postList.forEach(post -> {
            int postNum = post.getNum();
            int postPhotoCount = photoService.selectPhotoCountByPost(postNum);

            post.setPhotoCount(postPhotoCount);
            post.setPhotoViewGridSize(postPhotoCount);
            post.setHashtagList(hashtagService.selectHashtagByPost(postNum));

            List<Photo> photoList = photoService.selectPhotoByPost(postNum);

            Reply reply = new Reply();
            reply.setReplyCountByPost(replyService.selectReplyCountByPost(postNum));

            Likes likes = new Likes();
            likes.setLikesCountByPost(likesService.selectLikesCountByPost(postNum));

            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setUser(userService.selectUserByPost(postNum));
            userPhoto.setPhoto(photoService.selectPhotoByUser(userPhoto.getUser().getNum()));

            PostListFacadeDomain eachPostListFacadeDomain = PostListFacadeDomain.builder()
                    .post(post)
                    .photoList(photoList)
                    .mainPostPhoto(photoList.get(0))
                    .reply(reply)
                    .likes(likes)
                    .location(locationService.selectLocationByPost(postNum))
                    .userPhoto(userPhoto)
                    .build();

            postListFacadeDomainList.add(eachPostListFacadeDomain);
        });
    }

    private void SelectPostProcess() {
        postList.forEach(post -> {
            int postNum = post.getNum();
            int postPhotoCount = photoService.selectPhotoCountByPost(postNum);

            post.setPhotoCount(postPhotoCount);
            post.setPhotoViewGridSize(postPhotoCount);
            post.setHashtagList(hashtagService.selectHashtagByPost(postNum));

            List<Photo> photoList = photoService.selectPhotoByPost(postNum);

            Reply reply = new Reply();
            reply.setReplyCountByPost(replyService.selectReplyCountByPost(postNum));

            Likes likes = new Likes();
            likes.setLikesCountByPost(likesService.selectLikesCountByPost(postNum));

            UserPhoto userPhoto = new UserPhoto();
            userPhoto.setUser(userService.selectUserByPost(postNum));
            userPhoto.setPhoto(photoService.selectPhotoByUser(userPhoto.getUser().getNum()));

            PostListFacadeDomain eachPostListFacadeDomain = PostListFacadeDomain.builder()
                    .post(post)
                    .photoList(photoList)
                    .mainPostPhoto(photoList.get(0))
                    .reply(reply)
                    .likes(likes)
                    .location(locationService.selectLocationByPost(postNum))
                    .userPhoto(userPhoto)
                    .build();

            postListFacadeDomain = eachPostListFacadeDomain;
        });
    }


    public PostListFacadeDomain selectPostFacade(int num) {
        postList = postService.selectPost(num);
        SelectPostProcess();

        return postListFacadeDomain;
    }
}
