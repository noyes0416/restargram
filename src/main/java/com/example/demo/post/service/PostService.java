package com.example.demo.post.service;

import com.example.demo.post.domain.Post;
import com.example.demo.post.domain.PostDeleteFacadeDomain;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface PostService {
    List<Post> selectAllPostList(int userNum);

    List<Post> selectMyPostList(int userNum);

    void insertPost(Post post);

    Post selectPostDetail(Map hashMap);

    int getPostCount(int num);

    List<Post> selectPost(int num);

    void deletePost(PostDeleteFacadeDomain postDeleteFacadeDomain);

    int selectMyPostCount(int userNum);
}
