package com.example.demo.post.service;

import com.example.demo.post.domain.Post;
import com.example.demo.post.domain.PostDeleteFacadeDomain;
import com.example.demo.post.persistence.PostMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {

    private final PostMapper postMapper;

    public List<Post> selectAllPostList(int userNum) {
        return postMapper.selectAllPostList(userNum);
    };

    public List<Post> selectMyPostList(int userNum) {
        return postMapper.selectMyPostList(userNum);
    }

    public void insertPost(Post post) {
        postMapper.insertPost(post);
    }

    public Post selectPostDetail(Map hashMap) { return postMapper.selectPostDetail(hashMap); }

    public int getPostCount(int num){ return postMapper.getPostCount(num); }

    public List<Post> selectPost(int num){ return postMapper.selectPost(num);}

    public void deletePost(PostDeleteFacadeDomain postDeleteFacadeDomain) {
        postMapper.deletePost(postDeleteFacadeDomain);
    }

    public int selectMyPostCount(int userNum) {
        return postMapper.selectMyPostCount(userNum);
    }
}
