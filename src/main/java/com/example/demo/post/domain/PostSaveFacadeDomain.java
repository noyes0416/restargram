package com.example.demo.post.domain;

import com.example.demo.location.domain.Location;
import com.example.demo.photo.domain.Photo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Setter
@Getter
@Component
public class PostSaveFacadeDomain {
    private Post post;
    private Photo photo;
    private Location location;
    private List<MultipartFile> multipartFileList;

    public PostSaveFacadeDomain() {
        this.post = new Post();
        this.photo = new Photo();
        this.location = new Location();
    }
}
