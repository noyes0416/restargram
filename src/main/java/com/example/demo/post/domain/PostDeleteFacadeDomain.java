package com.example.demo.post.domain;

import com.example.demo.common.enumeration.ActiveStateEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Setter
@Getter
@Component
public class PostDeleteFacadeDomain {
    private int postNum;
    private final ActiveStateEnum activeStateEnum = ActiveStateEnum.INACTIVE;
    private final LocalDateTime updDt = LocalDateTime.now();
}
