package com.example.demo.post.domain;

import com.example.demo.likes.domain.Likes;
import com.example.demo.location.domain.Location;
import com.example.demo.photo.domain.Photo;
import com.example.demo.reply.domain.Reply;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.List;

@Setter
@Getter
@Builder
@Component
public class PostDetailFacadeDomain {
    private User user;
    private UserPhoto userPhoto;
    private Post post;
    private List<Photo> photoList;
    private List<Reply> replyList;
    private Likes likes;
    private Location location;
}
