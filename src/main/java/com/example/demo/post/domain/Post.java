package com.example.demo.post.domain;

import com.example.demo.common.enumeration.LikeTypeEnum;
import com.example.demo.likes.domain.Likes;
import com.example.demo.common.domain.Writing;
import org.springframework.stereotype.Component;

import static com.example.demo.common.domain.Constant.POST_PHOTO_VIEW_BASIC_GRID;

@Component
public class Post extends Writing {

    private final LikeTypeEnum LIKES_LOCATION_POST = LikeTypeEnum.POST;

    private int rowNum;
    private int postCountByLocation;
    private int postCountByHashtag;
    private boolean postLikedFlagByUser;
    private String photoViewGridSize;
    private int photoCount;

    public Likes getLikesFromPost(int userNum, int postNum) {
        Likes likes = new Likes();
        likes.setUserNum(userNum);
        likes.setLikeType(LIKES_LOCATION_POST);
        likes.setTypeValue(postNum);

        return likes;
    }

    public int getRowNum() {
        return this.rowNum;
    }

    public boolean getPostLikedFlagByUser() {
        return this.postLikedFlagByUser;
    }

    public String getPhotoViewGridSize() {
        return this.photoViewGridSize;
    }

    public int getPhotoCount() {
        return this.photoCount;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public void setPostCountByLocation(int count) {
        this.postCountByLocation = count;
    }

    public void setPostCountByHashtag(int count) {
        this.postCountByHashtag = count;
    }

    public void setPhotoViewGridSize(int photoCount) {
        this.photoViewGridSize = String.format("%dpx", ((photoCount - 1) * POST_PHOTO_VIEW_BASIC_GRID) - 1);
    }

    public void setPhotoCount(int photoCount) {
        this.photoCount = photoCount;
    }
}
