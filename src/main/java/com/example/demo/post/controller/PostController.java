package com.example.demo.post.controller;

import com.example.demo.follow.service.FollowService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.post.facade.PostDeleteFacade;
import com.example.demo.post.facade.PostDetailFacade;
import com.example.demo.post.facade.PostListFacade;
import com.example.demo.likes.service.LikesService;
import com.example.demo.post.domain.Post;
import com.example.demo.post.facade.PostSaveFacade;
import com.example.demo.post.service.PostService;
import com.example.demo.reply.domain.Reply;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.example.demo.common.domain.Constant.*;

@RequestMapping("/Post")
@RequiredArgsConstructor
@Controller
public class PostController {

    private final PostListFacade postListFacade;
    private final PostSaveFacade postSaveFacade;
    private final PostDetailFacade postDetailFacade;
    private final PostDeleteFacade postDeleteFacade;

    private final PostService postService;
    private final LikesService likesService;
    private final FollowService followService;

    @RequestMapping(value = "/List", method = RequestMethod.GET)
    public ModelAndView PostList(HttpServletRequest request) {
        ModelAndView mav = new ModelAndView( POST_LIST_VIEW_PATH);

        HttpSession session = request.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        int followingCount = followService.selectFollowingCount(userNum);
        int myPostCount = postService.selectMyPostCount(userNum);

        if (followingCount == 0 && myPostCount == 0)
        {
            mav.setViewName(USER_RECOMMENDED_REDIRECT_PATH);
        }
        else
        {
            mav.addObject("reply", new Reply());
            mav.addObject("RecommendInfos", followService.selectFollowInfo(userNum));
            mav.addObject("PostFacadeList", postListFacade.SelectAllPostListFacade(userNum));
            mav.addObject("postSaveFacadeDomain", new PostSaveFacadeDomain());
        }

        return mav;
    }

    @RequestMapping(value = "/Save", method = RequestMethod.POST)
    public String PostSave(@RequestParam("userNum") int userNum, @ModelAttribute("postSaveFacadeDomain") PostSaveFacadeDomain postSaveFacadeDomain) throws IOException {
        postSaveFacadeDomain.getPost().setUserNum(userNum);

        postSaveFacade.PostSave(postSaveFacadeDomain);

        return POST_LIST_REDIRECT_PATH;
    }

    @RequestMapping(value = "/Detail", method = RequestMethod.GET)
    public String GetDetail(HttpServletRequest request, @RequestParam("postnum") int postNum, Model model) {

        UserPhoto sessionUserInfo = (UserPhoto) request.getSession().getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();

        model.addAttribute("reply", new Reply());
        model.addAttribute("postDetailFacadeDomain",  postDetailFacade.SelectPostDetailFacade(userNum, postNum));

        return POST_DETAIL_MODAL_VIEW_PATH;
    }

    @RequestMapping(value = "/InsertLikes", method = RequestMethod.GET)
    public String InsertPostLikes(HttpServletRequest request, int postNum) {
        UserPhoto sessionUserInfo = (UserPhoto) request.getSession().getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        likesService.insertLikes(new Post().getLikesFromPost(userNum, postNum));

        return POST_LIST_REDIRECT_PATH;
    }

    @RequestMapping(value = "/DeleteLikes", method = RequestMethod.GET)
    public String DeleteLikesByPost(HttpServletRequest request, int postNum) {
        UserPhoto sessionUserInfo = (UserPhoto) request.getSession().getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        likesService.deleteLikes(new Post().getLikesFromPost(userNum, postNum));

        return POST_LIST_REDIRECT_PATH;
    }

    @ResponseBody
    @GetMapping(value = "/Delete")
    public void DeletePost(HttpServletRequest request, int postNum) {
        postDeleteFacade.PostDelete(postNum);
    }
}
