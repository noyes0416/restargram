package com.example.demo.follow.domain;

import com.example.demo.user.domain.UserPhoto;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Builder
//@Component
public class FollowInfo {
    private UserPhoto userPhoto;
    private boolean followYN;
}
