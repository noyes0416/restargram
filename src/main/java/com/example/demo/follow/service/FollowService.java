package com.example.demo.follow.service;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.follow.domain.Follow;
import com.example.demo.follow.domain.FollowInfo;
import com.example.demo.user.domain.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FollowService {
    List<User> selectFollowerList(int num);
    List<User> selectFollowingList(int num);
    int selectFollowerCount(int num);
    int selectFollowingCount(int num);
    void insertFollow(int sender, int receiver);
    void updateFollow(int sender, int receiver, int state);
    void deleteFollow(int sender, int receiver);
    List<FollowInfo> selectFollowInfo(int sender);
}
