package com.example.demo.follow.service.Impl;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.follow.domain.Follow;
import com.example.demo.follow.domain.FollowInfo;
import com.example.demo.follow.persistence.FollowMapper;
import com.example.demo.follow.service.FollowService;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.persistence.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class FollowServiceImpl implements FollowService {
    private final FollowMapper followMapper;
    private final UserMapper userMapper;
    private List<FollowInfo> followInfos;
    private List<UserPhoto> userPhotos;
    private List<Follow> follows;

    public List<User> selectFollowerList(int num) {
        return followMapper.selectFollowerList(num);
    };

    public List<User> selectFollowingList(int num ) {
        return followMapper.selectFollowingList(num);
    };

    public int selectFollowerCount(int num){ return followMapper.selectFollowerCount(num); };

    public int selectFollowingCount(int num){ return followMapper.selectFollowingCount(num); };

    @Transactional
    public void insertFollow(int sender, int receiver)
    {
         if(followMapper.selectFollowInfo(sender, receiver) == 0)
         {
            followMapper.insertFollow(sender, receiver);

         }
         else
         {
            updateFollow(sender, receiver, 1);
         }
    }

    public void updateFollow(int sender, int receiver, int state)
    {
        followMapper.updateFollow(sender, receiver, state);
    }

    public void deleteFollow(int sender, int receiver)
    {
        updateFollow(sender, receiver, 2);
    }

    public List<FollowInfo> selectFollowInfo(int sender)
    {
        followInfos = new ArrayList<>();

        userPhotos = userMapper.selectRecommendList(sender);

        userPhotos.forEach(userPhoto -> {
            boolean followYn = false;
            if (followMapper.selectFollowYn(sender, userPhoto.getUser().getNum()) != 0)
            {
                followYn = true;
            }

            FollowInfo followInfo = FollowInfo.builder()
                    .userPhoto(userPhoto)
                    .followYN(followYn)
                    .build();

            followInfos.add(followInfo);
        });

        return followInfos;
    }
}
