package com.example.demo.follow.controller;

import com.example.demo.follow.service.FollowService;
import com.example.demo.follow.domain.Follow;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequiredArgsConstructor
@Controller
public class FollowController {
    private final UserService userService;
    private final FollowService followService;

    @GetMapping("/Follow")
    public void Follow(HttpServletRequest httpServletRequest, String id)
    {
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int sender = user.getNum();
        int receiver = userService.selectUserNum(id);

        followService.insertFollow(sender, receiver);

        return;
    }

    @GetMapping("/UnFollow")
    public void UnFollow(HttpServletRequest httpServletRequest, String id)
    {
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int sender = user.getNum();
        int receiver = userService.selectUserNum(id);

        followService.deleteFollow(sender, receiver);

        return;
    }
}
