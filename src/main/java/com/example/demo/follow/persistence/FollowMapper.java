package com.example.demo.follow.persistence;

import com.example.demo.common.enumeration.ActiveStateEnum;
import com.example.demo.follow.domain.Follow;
import com.example.demo.user.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface FollowMapper {
    List<User> selectFollowerList(int num);
    List<User> selectFollowingList(int num);
    int selectFollowerCount(int num);
    int selectFollowingCount(int num);
    int selectFollowYn(int sender, int receiver);
    void insertFollow(int sender, int receiver);
    void updateFollow(int sender, int receiver, int state);
    int selectFollowInfo(int sender, int receiver);
}
