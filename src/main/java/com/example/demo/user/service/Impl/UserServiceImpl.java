package com.example.demo.user.service.Impl;

import com.example.demo.common.domain.Result;
import com.example.demo.common.enumeration.UserStateEnum;
import com.example.demo.follow.domain.Follow;
import com.example.demo.follow.persistence.FollowMapper;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.post.persistence.PostMapper;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.persistence.UserMapper;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserMapper userMapper;
    private final FollowMapper followMapper;
    private final PostMapper postMapper;
    private final PhotoService photoService;

    public List<User> selectUserList(int num)  {
        return userMapper.selectUserList(num);
    }

    public List<UserPhoto> selectRecommendList(int num)
    {

        return userMapper.selectRecommendList(num);
    }

    public int selectUserNum(String id)
    {
        return userMapper.selectUserNum(id);
    }

    public Map selectUserDetail(int sender, String id)
    {
        Map map = new HashMap<String, Object>();;
        int num = selectUserNum(id);

        User user = userMapper.selectUserDetail(num);
        int follower = followMapper.selectFollowerCount(num);
        int following = followMapper.selectFollowingCount(num);
        int post = postMapper.getPostCount(num);
        boolean followYn = false;
        if (followMapper.selectFollowYn(sender, num) != 0)
        {
            followYn = true;
        }

        List<User> followerList = followMapper.selectFollowerList(num);
        List<User> followingList = followMapper.selectFollowingList(num);

        map.put("userInfo", user);
        map.put("followerCount", follower);
        map.put("followerLists", followerList);
        map.put("followingCount", following);
        map.put("followingLists", followingList);
        map.put("postCount", post);
        map.put("followYn", followYn);
        map.put("userPhoto", photoService.selectPhotoByUser(num));

        return map;
    }

    public User selectSessionInfo(String email)
    {

        return userMapper.selectSessionInfo(email);
    }

    @Transactional
    public Result insertUser(User user)
    {
        Result result = new Result();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String decPassword = user.getPassword();
        String encPassword = encoder.encode(user.getPassword());
        user.setPassword(encPassword);

        int existUser = 0;

        try
        {
            existUser = userMapper.selectUserEmail(user.getEmail());
            if(existUser > 0)
            {
                result.setCode(301);
                result.setMessage("다른 계정에서 " + user.getEmail() + " 주소를 사용하고 있습니다.");
                return result;
            }

            existUser = userMapper.selectUserId(user.getId());
            if(existUser > 0)
            {
                result.setCode(302);
                result.setMessage("사용할 수 없는 사용자 이름입니다. 다른 이름을 사용하세요.");
                return result;
            }

            result.setCode(0);
            result.setMessage("ok");

            userMapper.insertUser(user);
            user.setPassword(decPassword);
            return result;
        }
        catch (Exception exception)
        {
            result.setCode(300);
            result.setMessage("오류가 발생했습니다. 다시 시도해주세요.");
            return result;
        }
    }

    @Transactional
    public Result updateSignIn(User user)
    {
        Result result = new Result();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        int existUser = 0;

        try
        {
            existUser = userMapper.selectUserEmail(user.getEmail());
            if(existUser == 0)
            {
                result.setCode(101);
                result.setMessage("입력한 이메일 주소를 사용하는 계정을 찾을 수 없습니다. 이메일 주소를 확인하고 다시 시도하세요.");
                return result;
            }

            User isUser = userMapper.selectIsUser(user);
            if (encoder.matches(user.getPassword(), isUser.getPassword()))
            {
                if(isUser.getState().equals(UserStateEnum.INACTIVE))
                {
                    result.setCode(102);
                    result.setMessage("사용할 수 없는 계정입니다.");
                    return result;
                }
            }
            else
            {
                result.setCode(103);
                result.setMessage("잘못된 비밀번호입니다. 다시 확인하세요.");
                return result;
            }

            result.setCode(0);
            result.setMessage("ok");
            userMapper.updateSignIn(isUser);
            return result;
        }
        catch (Exception exception)
        {
            result.setCode(100);
            result.setMessage("오류가 발생했습니다. 다시 시도해주세요.");
            return result;
        }
    }

    public User selectUserByPost(int postNum) {
        return userMapper.selectUserByPost(postNum);
    }
    public User selectUserInfo(int num){ return userMapper.selectUserDetail(num) ;}

    public void insertUserPhoto(UserPhoto userPhoto) throws IOException {
        photoService.insertUserPhoto(userPhoto);
    }

    public Result updateUser(User user, User bfUser)
    {
        Result result = new Result();
        try
        {
            if(user.getEmail().equals(bfUser.getEmail()) && user.getId().equals(bfUser.getId()))
            {
                result.setCode(0);
                result.setMessage("적용되었습니다.");
                return result;
            }

            if(!user.getEmail().equals(bfUser.getEmail()))
            {
                int existUser = userMapper.selectUserEmail(user.getEmail());
                if(existUser > 0)
                {
                    result.setCode(401);
                    result.setMessage("이미 등록된 이메일입니다.");
                    return result;
                }
            }

            if(user.getId().equals(bfUser.getId()))
            {
                int existUser = userMapper.selectUserId(user.getId());
                if(existUser > 0)
                {
                    result.setCode(402);
                    result.setMessage("이미 등록된 아이디입니다.");
                    return result;
                }
            }

            userMapper.updateUser(user);
        }
        catch (Exception exception)
        {
            result.setCode(400);
            result.setMessage("오류가 발생했습니다. 다시 시도해주세요.");
            return result;
        }

        return result;
    }
}
