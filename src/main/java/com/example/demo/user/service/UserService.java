package com.example.demo.user.service;

import com.example.demo.common.domain.Result;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@Service
public interface UserService {
    List<User> selectUserList(int num);
    Map selectUserDetail(int sender, String id);
    User selectSessionInfo(String email);
    Result insertUser(User user);
    Result updateSignIn(User user);
    List<UserPhoto> selectRecommendList(int num);
    int selectUserNum(String id);
    User selectUserByPost(int postNum);
    User selectUserInfo(int num);
    void insertUserPhoto(UserPhoto userPhoto) throws IOException;
    Result updateUser(User user, User bfUser);
}
