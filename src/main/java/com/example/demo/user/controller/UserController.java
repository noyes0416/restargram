package com.example.demo.user.controller;

import com.example.demo.common.domain.Result;
import com.example.demo.follow.service.FollowService;
import com.example.demo.photo.service.PhotoService;
import com.example.demo.post.domain.PostSaveFacadeDomain;
import com.example.demo.post.facade.PostListFacade;
import com.example.demo.post.service.PostService;
import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import com.example.demo.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;

import static com.example.demo.common.domain.Constant.*;

@RequiredArgsConstructor
@Controller
public class UserController {
    private final UserService userService;
    private final FollowService followService;
    private final PostListFacade postListFacade;
    private final PhotoService photoService;
    private final PostService postService;

    @GetMapping("/SignIn")
    public String SignIn()
    {
        return SIGNIN_VIEW_PATH;
    }

    @GetMapping( "/SignUp")
    public String SignUp(){

        return SIGNUP_VIEW_PATH;
    }

    @GetMapping("/SignOut")
    public String SignOut(HttpServletRequest httpServletRequest)
    {
        HttpSession session = httpServletRequest.getSession();
        session.invalidate();

        return SIGNIN_VIEW_REDIRECT_PATH;
    }

    @GetMapping("/User/Recommended")
    public String Recommended(Model model, HttpServletRequest httpServletRequest){
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();

        model.addAttribute("postSaveFacadeDomain", new PostSaveFacadeDomain());
        model.addAttribute("RecommendInfos", followService.selectFollowInfo(userNum));

        return USER_RECOMMENDED_VIEW_PATH;
    }

    @GetMapping("/User/Edit")
    public String Edit(Model model, HttpServletRequest httpServletRequest){
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();

        model.addAttribute("postSaveFacadeDomain", new PostSaveFacadeDomain());
        model.addAttribute("UserPhoto", photoService.selectPhotoByUser(userNum));

        return USER_EDIT_VIEW_PATH;
    }

    @PostMapping("User/Edit")
    public String Edit(@Valid User user, Model model, HttpServletRequest httpServletRequest)
    {
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User bfUser = sessionUserInfo.getUser();
        user.setNum(bfUser.getNum());

        Result result = userService.updateUser(user, bfUser);
        sessionUserInfo.setUser(user);

        model.addAttribute("message", result.getMessage());
        model.addAttribute("code", result.getCode());
        model.addAttribute("postSaveFacadeDomain", new PostSaveFacadeDomain());
        model.addAttribute("UserPhoto", photoService.selectPhotoByUser(user.getNum()));
        session.setAttribute("userInfo", sessionUserInfo);

        return USER_EDIT_VIEW_PATH;
    }

    @PostMapping("/SignIn")
    public String signIn(@Valid User user, Model model, HttpServletRequest httpServletRequest){
        Result result = userService.updateSignIn(user);
        User userInfo = userService.selectSessionInfo(user.getEmail());
        HttpSession session = httpServletRequest.getSession();
        UserPhoto userPhoto = new UserPhoto();

        userPhoto.setUser(userInfo);
        userPhoto.setPhoto(photoService.selectPhotoByUser(userInfo.getNum()));

        if(result.getCode() != 0){
            session.setAttribute("userInfo", null);
            model.addAttribute("message", result.getMessage());
            model.addAttribute("code", result.getCode());

            return SIGNIN_VIEW_PATH;
        }
        else{
            session.setAttribute("userInfo", userPhoto);

            int followingCount = followService.selectFollowingCount(userInfo.getNum());
            int myPostCount = postService.selectMyPostCount(userInfo.getNum());

            if (followingCount > 0 || myPostCount > 0)
            {
                return POST_LIST_REDIRECT_PATH;

            }
            else
            {
                return USER_RECOMMENDED_REDIRECT_PATH;
            }
        }
    }

    @PostMapping("/SignUp")
    public String signup(@Valid User user, Model model, HttpServletRequest httpServletRequest) {
        Result result = userService.insertUser(user);

        if(result.getCode() != 0)
        {
            model.addAttribute("message", result.getMessage());
            model.addAttribute("code", result.getCode());

            return SIGNUP_VIEW_PATH;
        }
        else
        {
            Result signInResult = userService.updateSignIn(user);
            if(signInResult.getCode() != 0)
            {
                model.addAttribute("message", signInResult.getMessage());
                model.addAttribute("code", signInResult.getCode());

                return SIGNIN_VIEW_PATH;
            }
            else
            {
                UserPhoto userPhoto = new UserPhoto();
                userPhoto.setUser(userService.selectSessionInfo(user.getEmail()));
                HttpSession session = httpServletRequest.getSession();

                session.setAttribute("userInfo", userPhoto);

                return USER_RECOMMENDED_REDIRECT_PATH;
            }
        }
    }

    @GetMapping("/User/Profile")
    public ModelAndView Profile(HttpServletRequest httpServletRequest, String id) {
        ModelAndView mav = new ModelAndView(USER_PROFILE_VIEW_PATH);

        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        int userNum = user.getNum();
        int num = userService.selectUserNum(id);

        mav.addObject("postSaveFacadeDomain", new PostSaveFacadeDomain());
        mav.addObject("ProfileInfo", userService.selectUserDetail(userNum, id));
        mav.addObject("PostFacadeList", postListFacade.SelectMyPostListFacade(userNum));
        mav.addObject("FollowingInfos", followService.selectFollowingList(num));
        mav.addObject("FollowerInfos", followService.selectFollowerList(num));

        return mav;
    }

    @PostMapping("/User/SaveImage")
    public String SaveImage(@Valid UserPhoto userPhoto, HttpServletRequest httpServletRequest) throws IOException {
        HttpSession session = httpServletRequest.getSession();
        UserPhoto sessionUserInfo = (UserPhoto) session.getAttribute("userInfo");
        User user = sessionUserInfo.getUser();
        userPhoto.setUser(user);

        userService.insertUserPhoto(userPhoto);
        userPhoto.setPhoto(photoService.selectPhotoByUser(user.getNum()));
        session.setAttribute("userInfo", userPhoto);

        return USER_EDIT_VIEW_PATH;
    }

}
