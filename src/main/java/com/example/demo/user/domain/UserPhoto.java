package com.example.demo.user.domain;

import com.example.demo.photo.domain.Photo;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

@Setter
@Getter
@Component
public class UserPhoto {
    private User user;
    private Photo photo;
    private MultipartFile multipartFile;
}
