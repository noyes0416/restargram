package com.example.demo.user.domain;

import com.example.demo.common.enumeration.UserStateEnum;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class User {
    private int num;
    private String email;
    private String id;
    private String name;
    private char agreementYn;
    private String password;
    private UserStateEnum state;
    private LocalDateTime lastLoginDt;
    private LocalDateTime regDt;
    private LocalDateTime updDt;

    public int getNum() {
        return this.num;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public char getAgreementYn()
    {
        return this.agreementYn;
    }

    public UserStateEnum getState()
    {
        return this.state;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getLastLoginDt()
    {
        if(this.lastLoginDt == null){
            return null;
        }
        else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return this.lastLoginDt.format(formatter);
        }
    }

    public String getRegDt()
    {
        if(this.regDt == null){
            return null;
        }
        else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return this.regDt.format(formatter);
        }

    }

    public String getUpdDt()
    {
        if(this.updDt == null){
            return null;
        }
        else{
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            return this.updDt.format(formatter);
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAgreementYn(char agreementYn) {
        this.agreementYn = agreementYn;
    }

    public void setState(UserStateEnum state){this.state = state;}
}
