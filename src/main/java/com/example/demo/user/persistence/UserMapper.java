package com.example.demo.user.persistence;

import com.example.demo.user.domain.User;
import com.example.demo.user.domain.UserPhoto;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;

import java.util.List;

@Mapper
public interface UserMapper {
    List<User> selectUserList(int num);
    User selectUserDetail(int num);
    User selectSessionInfo(String email);
    void insertUser(User user);
    User selectIsUser(User user);
    void updateSignIn(User user);
    int selectUserEmail(String email);
    int selectUserId(String id);
    List<UserPhoto> selectRecommendList(int num);
    int selectUserNum(String id);
    User selectUserByPost(int postNum);
    void updateUser(User user);
}
