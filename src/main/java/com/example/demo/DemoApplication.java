package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@SpringBootApplication
public class DemoApplication {
	@RequestMapping(value="/")
	public String main() { return "redirect:/SignIn"; }

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}