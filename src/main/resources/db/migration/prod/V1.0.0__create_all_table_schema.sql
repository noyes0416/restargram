CREATE TABLE IF NOT EXISTS User (
	 num		    INT             AUTO_INCREMENT
	,email		    VARCHAR(200)	NOT NULL
	,id		        VARCHAR(50)	    NOT NULL
	,name	        VARCHAR(50)	    NOT NULL
	,agreementYn	CHAR(1)		    NOT NULL
	,password	    VARCHAR(500)	    NOT NULL
	,state		    TINYINT		    NOT NULL
	,lastLoginDt	DATETIME	    NULL
	,regDt		    DATETIME	    NOT NULL
	,updDt		    DATETIME	    NULL
	,primary KEY(num)
);

CREATE TABLE IF NOT EXISTS Post (
    num     INT      NOT NULL AUTO_INCREMENT,
    userNum INT      NOT NULL,
    content TEXT,
    state   TINYINT  NOT NULL DEFAULT 1,
    regDt   DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt   DATETIME NULL,
    PRIMARY KEY (num)
);

CREATE TABLE IF NOT EXISTS Reply (
    num            INT      NOT NULL AUTO_INCREMENT,
    parentReplyNum INT      NULL,
    userNum        INT      NOT NULL,
    postNum        INT      NOT NULL,
    content        TEXT,
    state          TINYINT  NOT NULL DEFAULT 1,
    regDt          DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt          DATETIME NULL,
    PRIMARY KEY (num)
);
--CREATE INDEX Reply_postNum ON Reply(postNum);
--CREATE INDEX Reply_parentReplyNum ON Reply(parentReplyNum);

CREATE TABLE IF NOT EXISTS Likes (
    num       INT      NOT NULL AUTO_INCREMENT,
    userNum   INT      NOT NULL,
    likeType  TINYINT  NOT NULL COMMENT '1:게시글, 2:댓글, 3:DM',
    typeValue INT      NOT NULL COMMENT 'likeType별 num',
    PRIMARY KEY (num)
);
--CREATE INDEX Likes_likeTypeValue ON Likes(likeType, typeValue);

CREATE TABLE IF NOT EXISTS Hashtag (
    num   INT          NOT NULL AUTO_INCREMENT,
    value VARCHAR(200) NOT NULL UNIQUE,
    regDt DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (num)
);
--CREATE INDEX Hashtag_value ON Hashtag(value);

CREATE TABLE IF NOT EXISTS PostHashtag (
    postNum    INT NOT NULL,
    hashtagNum INT NOT NULL,
    PRIMARY KEY (postNum, hashtagNum)
);

CREATE TABLE IF NOT EXISTS Location (
    num   INT          NOT NULL AUTO_INCREMENT,
    value VARCHAR(200) NOT NULL UNIQUE,
    regDt DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (num)
);
--CREATE INDEX Location_value ON Location(value);

CREATE TABLE IF NOT EXISTS PostLocation (
    postNum     INT NOT NULL,
    locationNum INT NOT NULL,
    PRIMARY KEY (postNum, locationNum)
);

CREATE TABLE IF NOT EXISTS Follow (
	 num		    INT      not NULL auto_increment,
	 sender		    INT		 not null,
     receiver	    INT		 not null,
	 state			tinyint  not null,
	 regDt			DATETIME not null,
	 updDt			DATETIME,
	 primary key (NUM)
);

CREATE TABLE IF NOT EXISTS Message (
	 num		    int      not null auto_increment,
	 groupNum       int      not null,
	 sender		    int		 not null,
	 receiver		int		 not null,
	 postNum        int,
	 content        text     not null,
	 state			tinyint  not null,
	 viewDt		    datetime,
	 regDt			datetime not null,
	 updDt		    datetime,
	 primary key (num)
);

CREATE TABLE IF NOT EXISTS Photo (
    num        INT          NOT NULL AUTO_INCREMENT,
    originName VARCHAR(100) NOT NULL,
    fileName   VARCHAR(200) NOT NULL,
    filePath   VARCHAR(200) NOT NULL,
    photoType  TINYINT      NOT NULL COMMENT '1:User Profile, 2:Post Photo',
    state      TINYINT      NOT NULL DEFAULT 1,
    regDt      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt      DATETIME     NULL,
    PRIMARY KEY (num)
);

CREATE TABLE IF NOT EXISTS PostPhoto (
    postNum  INT NOT NULL,
    photoNum INT NOT NULL,
    PRIMARY KEY (postNum, photoNum)
);

CREATE TABLE IF NOT EXISTS UserPhoto (
    userNum  INT NOT NULL,
    photoNum INT NOT NULL,
    PRIMARY KEY (userNum, photoNum)
);

CREATE TABLE IF NOT EXISTS ReplyHashtag (
    replyNum   INT NOT NULL,
    hashtagNum INT NOT NULL,
    PRIMARY KEY (replyNum, hashtagNum)
);
