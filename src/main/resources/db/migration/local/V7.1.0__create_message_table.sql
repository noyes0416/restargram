CREATE TABLE IF NOT EXISTS Message
(
	 num		    int      not null auto_increment,
	 groupNum       int      not null,
	 sender		    int		 not null,
	 receiver		int		 not null,
	 postNum        int,
	 content        text     not null,
	 state			tinyint  not null,
	 viewDt		    datetime,
	 regDt			datetime not null,
	 updDt		    datetime,
	 primary key (num)
);


INSERT INTO message(GROUPNUM, sender, receiver, POSTNUM, CONTENT, STATE, VIEWDT, REGDT, UPDDT)
VALUES(1,1,2,NULL,"SPRING IS HERE!", 1, NOW(), NOW(), NOW());

INSERT INTO message(GROUPNUM, sender, receiver, POSTNUM, CONTENT, STATE, VIEWDT, REGDT, UPDDT)
VALUES(1,1,2,NULL,"HELLO WORLD", 1, NULL, NOW(), NULL);

