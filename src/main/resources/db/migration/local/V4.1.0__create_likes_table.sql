CREATE TABLE IF NOT EXISTS Likes (
    num       INT      NOT NULL AUTO_INCREMENT,
    userNum   INT      NOT NULL,
    likeType  TINYINT  NOT NULL COMMENT '1:게시글, 2:댓글, 3:DM',
    typeValue INT      NOT NULL COMMENT 'likeType별 num',
    PRIMARY KEY (num)
);

CREATE INDEX Likes_likeTypeValue ON Likes(likeType, typeValue);

-- Liked 1st Post by 1st User, 2nd User
INSERT INTO Likes(userNum, likeType, typeValue) VALUES(1, 1, 1);
INSERT INTO Likes(userNum, likeType, typeValue) VALUES(2, 1, 1);