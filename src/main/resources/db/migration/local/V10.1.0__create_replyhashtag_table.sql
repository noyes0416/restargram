-- ReplyHashtag
CREATE TABLE IF NOT EXISTS ReplyHashtag (
    replyNum   INT NOT NULL,
    hashtagNum INT NOT NULL,
    PRIMARY KEY (replyNum, hashtagNum)
);

INSERT INTO ReplyHashtag(replyNum, hashtagNum) VALUES(1, 1);
INSERT INTO ReplyHashtag(replyNum, hashtagNum) VALUES(1, 2);
INSERT INTO ReplyHashtag(replyNum, hashtagNum) VALUES(2, 2);