-- Photo
CREATE TABLE IF NOT EXISTS Photo (
    num        INT          NOT NULL AUTO_INCREMENT,
    originName VARCHAR(100) NOT NULL,
    fileName   VARCHAR(200) NOT NULL,
    filePath   VARCHAR(200) NOT NULL,
    photoType  TINYINT      NOT NULL COMMENT '1:User Profile, 2:Post Photo',
    state      TINYINT      NOT NULL DEFAULT 1,
    regDt      DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt      DATETIME     NULL,
    PRIMARY KEY (num)
);

INSERT INTO Photo(originName, fileName, filePath, photoType) VALUES('photo_1', 'photo_1', 'https://restagram-photo.s3.ap-northeast-2.amazonaws.com/fecbea41-e2ae-476c-9cda-048700d62203.jpeg', 2);
INSERT INTO Photo(originName, fileName, filePath, photoType) VALUES('photo_2', 'photo_2', 'https://restagram-photo.s3.ap-northeast-2.amazonaws.com/38da3590-b332-4b5a-8722-876fcad68552.png', 2);
INSERT INTO Photo(originName, fileName, filePath, photoType) VALUES('photo_2', 'photo_2', 'https://restagram-photo.s3.ap-northeast-2.amazonaws.com/cbd48532-3a42-4e1e-a00d-4cfa97c34387.jpeg', 2);
INSERT INTO Photo(originName, fileName, filePath, photoType) VALUES('photo_2', 'photo_2', 'https://restagram-photo.s3.ap-northeast-2.amazonaws.com/4388303a-bf96-4c70-851c-739bf739eba4.jpg', 2);

-- PostPhotos
CREATE TABLE IF NOT EXISTS PostPhoto (
    postNum  INT NOT NULL,
    photoNum INT NOT NULL,
    PRIMARY KEY (postNum, photoNum)
);

INSERT INTO PostPhoto(postNum, photoNum) VALUES(1, 1);
INSERT INTO PostPhoto(postNum, photoNum) VALUES(2, 2);
INSERT INTO PostPhoto(postNum, photoNum) VALUES(2, 3);
INSERT INTO PostPhoto(postNum, photoNum) VALUES(2, 4);
INSERT INTO PostPhoto(postNum, photoNum) VALUES(3, 3);
