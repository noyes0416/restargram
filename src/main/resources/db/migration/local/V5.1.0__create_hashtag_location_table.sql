-- Hashtag
CREATE TABLE IF NOT EXISTS Hashtag (
    num   INT          NOT NULL AUTO_INCREMENT,
    value VARCHAR(200) NOT NULL UNIQUE,
    regDt DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (num)
);

CREATE INDEX Hashtag_value ON Hashtag(value);

INSERT INTO Hashtag(value) VALUES('1st_Hashtag');
INSERT INTO Hashtag(value) VALUES('2nd_Hashtag');


-- PostHashtag
CREATE TABLE IF NOT EXISTS PostHashtag (
    postNum    INT NOT NULL,
    hashtagNum INT NOT NULL,
    PRIMARY KEY (postNum, hashtagNum)
);

INSERT INTO PostHashtag(postNum, hashtagNum) VALUES(1, 1);
INSERT INTO PostHashtag(postNum, hashtagNum) VALUES(1, 2);


-- Location
CREATE TABLE IF NOT EXISTS Location (
    num   INT          NOT NULL AUTO_INCREMENT,
    value VARCHAR(200) NOT NULL UNIQUE,
    regDt DATETIME     NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (num)
);

CREATE INDEX Location_value ON Location(value);

INSERT INTO Location(value) VALUES('1st Location');
INSERT INTO Location(value) VALUES('2nd Location');


-- PostLocation
CREATE TABLE IF NOT EXISTS PostLocation (
    postNum     INT NOT NULL,
    locationNum INT NOT NULL,
    PRIMARY KEY (postNum, locationNum)
);

INSERT INTO PostLocation(postNum, locationNum) VALUES(1, 1);
INSERT INTO PostLocation(postNum, locationNum) VALUES(2, 2);
INSERT INTO PostLocation(postNum, locationNum) VALUES(3, 1);