CREATE TABLE IF NOT EXISTS UserPhoto(
    userNum  INT NOT NULL,
    photoNum INT NOT NULL,
    PRIMARY KEY (userNum, photoNum)
);

--INSERT INTO userPhoto(userNum, photoNum) VALUES(1, 1);