CREATE TABLE IF NOT EXISTS Follow
(
	 num		    INT      not NULL auto_increment,
	 sender		    INT		 not null,
     receiver	    INT		 not null,
	 state			tinyint  not null,
	 regDt			DATETIME not null,
	 updDt			DATETIME,
	 primary key (NUM)
);

INSERT INTO follow(sender, receiver, state, regDt, updDt) values (1,1,1,now(), null);
INSERT INTO follow(sender, receiver, state, regDt, updDt) values (1,2,1,now(), null);
INSERT INTO follow(sender, receiver, state, regDt, updDt) values (7,1,1,now(), null);