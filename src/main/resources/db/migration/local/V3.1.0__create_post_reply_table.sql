CREATE TABLE IF NOT EXISTS Post (
    num     INT      NOT NULL AUTO_INCREMENT,
    userNum INT      NOT NULL,
    content TEXT,
    state   TINYINT  NOT NULL DEFAULT 1,
    regDt   DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt   DATETIME NULL,
    PRIMARY KEY (num)
);

CREATE TABLE IF NOT EXISTS Reply (
    num            INT      NOT NULL AUTO_INCREMENT,
    parentReplyNum INT      NULL,
    userNum        INT      NOT NULL,
    postNum        INT      NOT NULL,
    content        TEXT,
    state          TINYINT  NOT NULL DEFAULT 1,
    regDt          DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updDt          DATETIME NULL,
    PRIMARY KEY (num)
);

CREATE INDEX Reply_postNum ON Reply(postNum);
CREATE INDEX Reply_parentReplyNum ON Reply(parentReplyNum);

INSERT INTO Post(userNum, content) VALUES(1, '%s 1st Post %s');
INSERT INTO Post(userNum, content) VALUES(1, '2nd Post');
INSERT INTO Post(userNum, content) VALUES(1, '3rd Post');

INSERT INTO Reply(parentReplyNum, userNum, postNum, content) VALUES(null, 1, 1, '첫번째 유저의 첫번째 댓글');
INSERT INTO Reply(parentReplyNum, userNum, postNum, content) VALUES(null, 2, 1, '두번째 유저의 첫번째 댓글');
INSERT INTO Reply(parentReplyNum, userNum, postNum, content) VALUES(1, 1, 1, '첫번째 유저의 첫번째 글에 대한 대댓글');
INSERT INTO Reply(parentReplyNum, userNum, postNum, content) VALUES(1, 2, 1, '두번째 유저의 첫번째 글에 대한 대댓글');