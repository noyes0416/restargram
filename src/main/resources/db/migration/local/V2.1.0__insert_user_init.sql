USE Restagram;

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
           values('one@google.com', 'one_1', 'one', 'Y', 'qwER'
                 ,'0', null, now(), null);

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
          values('two@google.com', 'two_1', '22', 'Y', 'qwER'
                 ,'0', null, now(), null);

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
           values('three@google.com', 'three_1', 'three', 'Y', 'qwER'
                 ,'0', null, now(), null);

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
           values('55@google.com', 'bobo', 'Bo', 'Y', 'qwER'
                 ,'0', null, now(), null);

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
           values('20220118@google.com', 'today', 'die', 'Y', 'qwER'
                 ,'0', null, now(), null);

insert into user(email, id, name, agreementYn, password
                ,state, lastLoginDt, regDt, updDt)
           values('4444444444444444@google.com', '4444', 'rip', 'Y', 'qwER'
                 ,'0', null, now(), null);