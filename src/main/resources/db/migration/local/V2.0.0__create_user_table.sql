CREATE TABLE IF NOT EXISTS User (
	 num		    INT             AUTO_INCREMENT
	,email		    VARCHAR(200)	NOT NULL
	,id		        VARCHAR(50)	    NOT NULL
	,name	        VARCHAR(50)	    NOT NULL
	,agreementYn	CHAR(1)		    NOT NULL
	,password	    VARCHAR(500)	    NOT NULL
	,state		    TINYINT		    NOT NULL
	,lastLoginDt	DATETIME	    NULL
	,regDt		    DATETIME	    NOT NULL
	,updDt		    DATETIME	    NULL
	,primary KEY(num)
)