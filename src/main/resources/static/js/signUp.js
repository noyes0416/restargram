const inputEmail = document.getElementById('email');
const inputPassword = document.getElementById('password');
const inputName = document.getElementById('name');
const inputId = document.getElementById('id');
const divSignUp = document.getElementById('divSignUp');
const btnSignUp = document.getElementById('btnSignUp');

function emailCheck() {
    var hasAt = inputEmail.value.indexOf('@');
    return hasAt !== -1 ? true : false;
}

function pwCheck() {
    return inputPassword.value.length >= 4 ? true : false;
}

function nameCheck() {
    return inputName.value.length >= 4 ? true : false;
}

function idCheck() {
    return inputId.value.length >= 4 ? true : false;
}

divSignUp.addEventListener('keyup', function(event) {
    const completedInput = (emailCheck() && pwCheck() && nameCheck() && idCheck());
    btnSignUp.disabled = completedInput ? false : true;
})