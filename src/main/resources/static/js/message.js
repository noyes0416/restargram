const inputMessage = document.getElementById('content');
const divSendMessage = document.getElementById('divSendMessage');
const btnSendMessage = document.getElementById('btnSendMessage');

function messageCheck() {
    return inputMessage.value.length >= 1 ? true : false;
}

divSendMessage.addEventListener('keyup', function(event) {
    const completedInput = (messageCheck());
    btnSendMessage.disabled = completedInput ? false : true;
})