const inputEmail = document.getElementById('email');
const inputPassword = document.getElementById('password');
const divSignIn = document.getElementById('divSignIn');
const btnSignIn = document.getElementById('btnSignIn');

function emailCheck() {
    var hasAt = inputEmail.value.indexOf('@');
    return hasAt !== -1 ? true : false;
}

function pwCheck() {
    return inputPassword.value.length >= 4 ? true : false;
}

divSignIn.addEventListener('keyup', function(event) {
    const completedInput = (emailCheck() && pwCheck());
    btnSignIn.disabled = completedInput ? false : true;
})